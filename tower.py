# tower.py {{{1
# Andy Hammerlindl  2008/02/09
#
# Entry for the 2008 Game Making Deathmatch.  Build a tower that can send deadly
# viruses into dimension N+1.

# Imports {{{1
from __future__ import with_statement

import os
import sys
import socket
import errno
import pickle as pickle
import struct
import shelve
import math
import random
from math import sqrt, exp, log, sin, cos, tan, asin, acos, atan, atan2
from operator import add, concat

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GL.ARB.texture_rectangle import *
import pygame
from pygame.locals import *

# Constants {{{1
fullscreen = True

width = 1024
height = 768
#width = 1680
#height = 1050

# Update the world atmost every 50 ms.
updateThreshold = 50

# How fast partially construced buildings fall into disrepair.
disrepairFactor = 1.0/3.0

# How long a building is highlighted when it dies.
rubbleDuration = 500

# The dimensions of each level of the tower is.
levelHeight = 120
baseHeight = levelHeight - 64
baseLength = 400
levelStep = 40

# Where the pillars holding up the girders are placed.  This is needed by the
# Girder class to draw the pillars and by the Level class to know where to place
# building between pillars.
pillarWidth = 16
pillarInset = 0.15

mountLength = 40
mountHeight = 40

# How long it take a heated gun to cool down.
coolDownTime = 15000

# The acceleration of gravity, in pixels per ticks^2
gravity = 5 * 10 ** -4

# Colors for drawing buildings.
blueprintColor = (0,0,0,0.2)
buildingColor = (0,0,0,0.5)
builtColor = (0,0,0,1)
damagedColor = (0.4, 0, 0, 1)
rubbleColor = (2.0, 0.5, 0, 1)
hotColor = (1.0, 0.5, 0.0, 1)

missileColor = (1,1,1,1)

# Virus have different levels, each with their own color.
virusColors = [ (1.0, 0.6, 1.0, 1.0), (1.0, 1.0, 0.5, 1.0),
                (0.6, 1.0, 0.6, 1.0), (1.0, 0.8, 0.6, 1.0) ]
foreignVirusColor = (0.6, 0.85, 1.0, 1.0)
gatewayLoadTime = 4000
mutatorHalfLife = 5000

virusTravelTime = 8000
virusDecayTime = 80

virusOverPercent = float(virusDecayTime) / float(virusTravelTime)


# The rate at which clouds rise
lift = 0.01

# Draw Layer - To draw all parts of the scene in the right order.  Lower
# numbers are drawn first.
BACKDROP_LAYER = -99
SPLINE_LAYER = -3
VIRUS_LAYER = -2
CONTRAIL_LAYER = -1
BUILDING_LAYER = 0
FLAME_LAYER = 1
AURA_LAYER = 2
MISSILE_LAYER = 3
CLOUD_LAYER = 4
LABEL_LAYER = 99

# Events
PURGE_EVENT = USEREVENT + 1
MUSIC_EVENT = USEREVENT + 2

# Stats {{{1

# buildingStats holds the stats that are shared for each building of a certain
# type, such as the time to build it, and it's maximum hit points.
buildingStats = {
        'factory': { 'hp': 1000, 'buildtime': 8000,
            'desc': 'Improves building times.' },
        'plant': { 'hp': 1000, 'buildtime': 7000,
            'desc': 'Improves shield effectiveness.' },
        'munitions': { 'hp': 1000, 'buildtime': 7000,
            'desc': 'Improves missile accuracy and yield.' },
        'gateway': { 'hp': 800, 'buildtime': 9000,
            'desc': 'Sends viruses through dimension N+1.' },
        'mutator': { 'hp': 800, 'buildtime': 7000,
            'desc': 'Mutates viruses to counter immunity.' },
        'girder': { 'hp': 2000, 'buildtime': 1000 }, # Shouldn't be used.
        'gun': { 'hp': 400, 'buildtime': 3000,
            'desc': 'Fires missiles.' },
        'shield': { 'hp': 400, 'buildtime': 3000,
            'desc': 'Deflects missiles.' }
        }

# Stats for each level:
levelStats = [
        { 'girder': { 'buildtime': 10000, 'hp': 5000 },
          'buildings': [ 'Factory', 'Factory' ] },
        { 'girder': { 'buildtime': 12000, 'hp': 4000 },
          'buildings': [ 'Factory', 'Plant' ] },
        { 'girder': { 'buildtime': 16000, 'hp': 2800 },
          'buildings': [ 'Factory', 'Munitions' ] },
        { 'girder': { 'buildtime': 32000, 'hp': 1400 },
          'buildings': [ 'Gateway', 'Plant' ] },
        { 'girder': { 'buildtime': 45000, 'hp': 1200 },
          'buildings': [ 'Mutator', 'Munitions' ] },
        { 'girder': { 'buildtime': 90000, 'hp': 1000 },
          'buildings': [ 'Plant', 'Munitions' ] } ]

# Factory boost - rate that building is increased by for a tower with factories.
factoryBoost = [ 1.0, 1.35, 1.70, 1.95 ]

# Missile Stats - stats are improved by building munitions.
# For accuracy, lower is better.
baseRange = width - 1024 + 400
missileStats = [
        { 'range': (baseRange,150), 'angle': 30.0, 'accuracy': 0.07,
          'radius': 120, 'damage': 200, 'scale': 1.0,
          'loadTime': 1000, 'heat': 0.5 },
        { 'range': (baseRange+50,200), 'angle': 27.0, 'accuracy': 0.06,
            'radius': 135, 'damage': 300, 'scale': 1.15,
          'loadTime': 750, 'heat': 0.4 },
        { 'range': (baseRange+100,300), 'angle': 25.0, 'accuracy': 0.04,
            'radius': 150, 'damage': 400, 'scale': 1.25,
          'loadTime': 600, 'heat': 0.27 },
        { 'range': (baseRange+200,400), 'angle': 20.0, 'accuracy': 0.02,
            'radius': 165, 'damage': 500, 'scale': 1.4,
          'loadTime': 400, 'heat': 0.2 } ]

shieldStats = [
        { 'radius': 200, 'strength': 700, 'loadTime': 3000, 'width': 15.0 },
        { 'radius': 220, 'strength': 900, 'loadTime': 2500, 'width': 20.0 },
        { 'radius': 240, 'strength': 1150, 'loadTime': 2000, 'width': 28.0 },
        { 'radius': 250, 'strength': 1400, 'loadTime': 1500, 'width': 35.0 } ]

# Shelf {{{1
shelf = shelve.open("settings")
def defaultSetting(name, value):
    if name not in shelf:
        shelf[name] = value

defaultSetting('graphicsLevel', 2)
defaultSetting('address', '127.0.1.1')
defaultSetting('handicap', 1.0)
defaultSetting('aiHandicap', 1.0)
defaultSetting('lastPlayed', None)
defaultSetting('fullscreen', True)

def graphicsLevel():
    return shelf['graphicsLevel']

def toggleGraphicsLevel():
    level = graphicsLevel()
    if level < 3:
        shelf['graphicsLevel'] = level+1
    else:
        shelf['graphicsLevel'] = 1


def graphicsDependent(*array):
    return array[graphicsLevel() - 1]

# Util Functions {{{1
def interp(t, a, b):
    if type(a) == tuple:
        assert type(b) == tuple and len(a) == len(b)
        return tuple([interp(t, a[i], b[i]) for i in range(len(a))])
    else:
        return a + t * (b-a)

def insort(a, x, key, lo=0, hi=None):
    """Insert x into the list a, using key to determine the order.  This assumes
    that a is already sorted.  x goes to the right-most position.  This is based
    on the bisect module (which doesn't support keys)."""
    if hi is None:
        hi = len(a)
    while lo < hi:
        mid = (lo+hi)//2
        if key(x) < key(a[mid]): hi = mid
        else: lo = mid+1
    a.insert(lo, x)

def argmin(sequence, fn):
    """Return the element x of sequence so that fn(x) is minimal.  Based on
    Peter Norvig's elementations:
    http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/389640"""
    return min((fn(x), x) for x in sequence)[1]

def solveQuadratic(a, b, c):
    desc = b**2 - 4*a*c

    if desc < 0.0:
        return []

    root = sqrt(desc)
    return [(-b-root) / (2*a), (-b+root) / (2*a)]
    
def invertPos(pos):
    x,y = pos
    return (x, height-y-1)

def mousePos():
    return invertPos(pygame.mouse.get_pos())

def bboxCenter(bbox):
    l,b,r,t = bbox
    return ((r+l)/2, (t+b)/2)

def dist2(loc1, loc2):
    x1,y1 = loc1; x2,y2 = loc2
    return (x1-x2)**2 + (y1-y2)**2

def dist(loc1, loc2):
    return sqrt(dist2(loc1, loc2))

def length2(vector):
    x,y = vector
    return x**2 + y**2

def length(vector):
    x,y = vector
    return sqrt(x**2 + y**2)

def unit(vector):
    x,y = vector
    if x == 0.0 and y == 0.0:
        return (0,0)
    else:
        l = length(vector)
        return (x/l, y/l)

def dot(u, v):
    return u[0]*v[0] + u[1]*v[1]

def locationInBBox(location, bbox):
    x,y = location
    l,b,r,t = bbox
    return l <= x and x < r and b <= y and y < t

def bboxInBBox(box1, box2):
    return locationInBBox((box1[0], box1[1]), box2) and \
            locationInBBox((box1[2], box1[3]), box2)

def distToRange(x, lo, hi):
    if x < lo:
        return lo - x
    elif x > hi:
        return x - hi
    else:
        return 0
    
def distToBBox(location, bbox):
    x,y = location; l,b,r,t = bbox
    return length((distToRange(x,l,r), distToRange(y,b,t)))

def sideOfScreen(location):
    if location[0] < width/2:
        return 'left'
    else:
        return 'right'

class NoTexture():
    def __enter__(self):
        glDisable(textureFlag)
    def __exit__(self, type, value, tb):
        glEnable(textureFlag)

class PushedMatrix():
    def __enter__(self):
        glPushMatrix()
    def __exit__(self, type, value, tb):
        glPopMatrix()

class glGroup():
    def __init__(self, mode):
        self.mode = mode
    def __enter__(self):
        glBegin(self.mode)
    def __exit__(self, type, value, tb):
        glEnd()

class Perspective():
    def __init__(self, center=(width/2,height/2), stop=width/2):
        self.center = center
        self.stop = stop
    def __enter__(self):
        x0, y0 = self.center
        l = self.stop
        glMatrixMode(GL_PROJECTION)
        glPushMatrix()

        # Matrices are given in a weird order in OpenGL, so it is actually the
        # transpose of this matrix.
        glMultMatrixf([    l,     0,  0,  0,
                           0,     l,  0,  0,
                          x0,    y0,  0,  1,
                       -x0*l, -y0*l,  0,  0])

        glMatrixMode(GL_MODELVIEW)

    def __exit__(self, type, value, tb):
        glMatrixMode(GL_PROJECTION)
        glPopMatrix()
        glMatrixMode(GL_MODELVIEW)

def drawRectangle(bbox, color):
    with NoTexture():
        glColor4fv(color)

        l,b,r,t = bbox
        glBegin(GL_QUADS)
        glVertex2f(l,b)
        glVertex2f(r,b)
        glVertex2f(r,t)
        glVertex2f(l,t)
        glEnd()

def drawRoundedRectangle(bbox, color, border=8, segments = 24):
    segments /= 4

    def arc(x,y, angle1, angle2):
        for i in range(segments+1):
            angle = angle1 + float(i)/float(segments) * (angle2 - angle1)
            glVertex2f(x + border*cos(angle), y + border*sin(angle))

    with NoTexture():
        glColor4fv(color)
        l,b,r,t = bbox
        l += border/2; r -= border/2
        b += border/2; t -= border/2

        glBegin(GL_POLYGON)
        arc(l, b, -math.pi, -0.5*math.pi)
        arc(r, b, -0.5*math.pi, 0.0)
        arc(r, t, 0.0, 0.5*math.pi)
        arc(l, t, 0.5*math.pi, math.pi)
        glEnd()

def otherSide(side):
    if side == 'left':
        return 'right'
    else:
        assert side == 'right'
        return 'left'

# A list of functions to be called after OpenGL is initialized.
postGLInits = []

# Network {{{1
PORT = 6729
class Network:
    """This classes encapsulates the sockets used to send messages send between
    computers.  Use the methods sendMessage(msg) and getMessages() to send and
    receive messages.  Messages are simply dictionaries.  getMessages() never
    blocks and returns either a complete message or None."""

    def __init__(self):
        self.sizeBuf = ''
        self.size = None
        self.buf = ''
        self.LSize = struct.calcsize("l")
        self.name = 'offline'

    def reset(self):
        if 'server' in self.__dict__:
            self.server.close()
            del self.server
        if 'client' in self.__dict__:
            self.client.close()
            del self.client
        if 'sock' in self.__dict__:
            self.sock.close()
            del self.sock
        self.name = 'offline'

        # Should we reset the message state?

    def startServer(self):
        """Starts the server and returns."""
        assert 'server' not in self.__dict__
        assert 'sock' not in self.__dict__
        assert 'client' not in self.__dict__

        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setblocking(0)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind(('', PORT))
        #self.server.bind((socket.gethostname(), PORT))
        self.server.listen(5)

    def connectToClient(self):
        """Once the server is started, listen for a client.  This never blocks
        and returns a True if a client is connected."""
        assert 'server' in self.__dict__
        assert 'sock' not in self.__dict__
        assert 'client' not in self.__dict__

        try:
            (sock, address) = self.server.accept()
            sock.setblocking(0)
            self.sock = sock
            self.name = 'server'

            self.server.close();
            del self.server
            return True
        except socket.error, e:
            # Only handle blocking errors.  Rethrow anything else.
            if  e.args[0] == errno.EWOULDBLOCK:
                return False
            else:
                raise

    def connectToServer(self, address):
        """Connect to the server.  This doesn't block and return True if a
        connection is established.  Can be called repeatedly (but with the same
        address)."""
        assert 'server' not in self.__dict__
        assert 'sock' not in self.__dict__

        if 'client' not in self.__dict__:
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client.setblocking(0)

        # Based on code in the asyncore module.
        err = self.client.connect_ex((address, PORT))
        if err in (errno.EINPROGRESS, errno.EALREADY, errno.EWOULDBLOCK):
            return False
        if err in (0, errno.EISCONN):
            self.sock = self.client
            self.name = 'client'

            del self.client
            return True
        else:
            raise socket.error, (err, os.strerror(err))

    def sendMessage(self, obj):
        """Send an object through the network.  Based on the example:
        http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/457669/index_txt
        """
        assert 'sock' in self.__dict__

        #print "sending:", obj
        buf = pickle.dumps(obj)
        size = struct.pack("l", socket.htonl(len(buf)))
        self.sock.send(size)
        self.sock.send(buf)

    def getMessage(self):
        """Returns the next message in the queue that has been completely sent
        through the network.  Returns None if no such message is available."""

        try:
            if self.size == None:
                # Read in the size until it is completely read.
                while len(self.sizeBuf) < self.LSize:
                    feed = self.sock.recv(self.LSize - len(self.sizeBuf))
                    if feed == '':
                        return None
                    else:
                        self.sizeBuf += feed

                # Decode the integer size from the sizeBuf string.
                assert len(self.sizeBuf) == self.LSize
                self.size = socket.ntohl(struct.unpack("l", self.sizeBuf)[0])
                self.sizeBuf = ''

            # If we've reached here, we have a size.
            assert self.size != None

            # Read in the buffer until it is completely read.
            while len(self.buf) < self.size:
                feed = self.sock.recv(self.size - len(self.buf))
                if feed == '':
                    return None
                else:
                    self.buf += feed

            # Unpack the object from the buffer.
            assert len(self.buf) == self.size
            obj = pickle.loads(self.buf)
            self.buf = ''
            self.size = None
            #print "received:", obj
            return obj
        except socket.error, e:
            if e[0] in [errno.EAGAIN, errno.EWOULDBLOCK]:
                return None
            else:
                raise

# The global instance of the network.
net = Network()

def closeSocket():
    if 'server' in net.__dict__ and net.server:
        net.server.close()
    if 'client' in net.__dict__ and net.client:
        net.client.close()
    if 'sock' in net.__dict__ and net.sock:
        net.sock.close()

sys.exitfunc = closeSocket

# Entities {{{1
class Entity:
    def local(self):
        """Returns true if this computer is manager the player who owns this
        entity.  Uses the side field."""
        return self.side in towers

    def tower(self):
        return towers[self.side]

    def playing(self):
        return self.local() and self.tower().playing

    def broadcast(self, method, **msg):
        """Broadcast sends a message out to every computer, including the local
        one."""
        assert 'name' not in msg or msg['name'] == self.name
        assert 'method' not in msg or msg['method'] == method

        msg['name'] = self.name
        msg['method'] = method

        broadcastMessage(msg)

    def emit(self, method, **msg):
        """Emit sends a message to the other computer, but does not process it
        locally."""
        assert 'name' not in msg or msg['name'] == self.name
        assert 'method' not in msg or msg['method'] == method

        msg['name'] = self.name
        msg['method'] = method

        emitMessage(msg)

# Global repository of all entities.
entities = {}

nameCounter = 0
def newName():
    global nameCounter
    nameCounter += 1
    return (net.name, nameCounter)

def entityFromMessage(msg):
    assert msg['method'] == 'create'
    assert msg['name'] not in entities

    
    # Get the class object of the entity we are creating.
    cls = globals()[msg['type']]
    name = msg['name']

    entity = cls(**msg)
    entity.name = name
    entities[name] = entity
    return entity
    
def nameof(cls):
    """Replace a class object with it's name.  Leave anything else unchanged."""
    return getattr(cls, '__name__', cls)

def createEntity(cls, **msg):
    type = nameof(cls)

    assert 'method' not in msg or msg['method'] == 'create'
    assert 'type' not in msg or msg['type'] == type
    assert 'name' not in msg

    msg['name'] = newName()
    msg['method'] = 'create'
    msg['type'] = type

    entity = entityFromMessage(msg)
    if net.name != 'offline':
        net.sendMessage(msg)
    return entity
    
def processMessage(msg):
    if msg['method'] == 'create':
        entityFromMessage(msg)
    else:
        entity = entities[msg['name']]
        method = getattr(entity.__class__, msg['method'])
        method(entity, **msg)
        
def processPendingMessages():
    if net.name != 'offline':
        while 1:
            msg = net.getMessage()
            if msg == None:
                break
            processMessage(msg)

def emitMessage(msg):
    if net.name != 'offline':
        net.sendMessage(msg)

def broadcastMessage(msg):
    emitMessage(msg)
    processMessage(msg)


# PriorityList {{{1
class PriorityList:
    """PriorityList maintains a list of objects in an ordering prescribed by the
    level given when the object is added.  The objects are ordered by lowest
    level first.  The list can be accessed by the objs field."""
    def __init__(self):
        self.objs = []

    def add(self, obj, level):
        """Add a drawable object at a specified level."""
        insort(self.objs, (obj, level), key = lambda x: x[1])

    def remove(self, target):
        for i in range(len(self.objs)):
            obj, level = self.objs[i]
            if obj == target:
                del self.objs[i]
                return

# Updater {{{1
class Updater(PriorityList):
    def __init__(self):
        PriorityList.__init__(self)
        self.paused = False
        self.pauseTime = None
        self.downTime = 0 # Number of ticks paused since the beginning of time.

        self.pauseLabel = None

    def update(self, ticks, duration):
        """Update every object that needs to be updated."""
        if not self.paused:
            # Use a copy so objects can be removed while updating.
            for (obj, level) in self.objs[:]:
                obj.update(ticks - self.downTime, duration)

    def duration(self, t1, t2):
        """Returns the duration of an interval where times is given in pygame
        ticks.  This accounts for pausing."""
        assert t1 <= t2
        if self.paused:
            if t1 > self.pauseTime:
                return 0
            elif t2 > self.pauseTime:
                return t2 - self.pauseTime
        return t2 - t1
                
    def convert(self, ticks):
        if self.paused and ticks > self.pauseTime:
            ticks = self.pauseTime
        return ticks - self.downTime

    def get_ticks(self):
        return self.convert(pygame.time.get_ticks())
    
    def pause(self):
        if self.paused:
            return

        self.paused = True
        self.pauseTime = pygame.time.get_ticks()

        if self.pauseLabel == None:
            self.pauseLabel = Label('Paused', (width/2,height/2), 'center')
        else:
            self.pauseLabel.text = 'Paused'
        title.show()

    def unpause(self):
        if not self.paused:
            return

        self.downTime += pygame.time.get_ticks() - self.pauseTime
        self.pauseTime = None

        if self.pauseLabel:
            self.pauseLabel.text=''

        self.paused = False
        title.hide()

    def togglePause(self):
        if self.paused:
            self.unpause()
        else:
            self.pause()
updater = Updater()

class PauseMessage(Entity):
    def __init__(self, pause, **msg):
        if pause:
            updater.pause()
        else:
            updater.unpause()

# Scene {{{1
class Scene(PriorityList):
    """To make it easy to have special effects such as explosions, all drawing
    is managed in a central place.  The scene maintains a list of drawable
    objects (sorted by level).  To draw everything, the scene calls the draw
    method of every object in its list, which draws them in increasing order.
    
    Draw objects must support a draw() method (this will be passed timing info
    eventually), and a doneDrawing() method which returns a boolean telling the
    drawManager if it can remove the object from its list."""

    def draw(self):
        """Draw the entire scene."""
        for (obj, level) in self.objs:
            obj.draw()

    def purge(self):
        """Remove any animations or objects that no longer need to be drawn."""
        self.objs = [pair for pair in self.objs if not pair[0].doneDrawing()]

scene=Scene()

# Mixer {{{1
soundList = [ 'building', 'built', 'missile', 'loading', 'loaded',
        'loading_shield', 'loaded_shield', 'nogo',
        'loading_virus', 'loaded_virus', 'launch_virus', 'virus',
        'mutator', 'mutated', 'click', 'victory'
        ]
explodeList= [ 'explode'+str(i) for i in range(5) ]
destroyList = [ 'destroy'+str(i) for i in range(2) ]
soundList += explodeList + destroyList

musicList = (['music'+str(i)+'.ogg' for i in range(3) ])

class Mixer:
    def __init__(self):
        self.sounds = {}
        for name in soundList:
            self.load(name)

        self.sounds['click'].set_volume(0.15)

        random.shuffle(musicList)

        pygame.mixer.init(44100)
        pygame.mixer.music.set_volume(0.7)
        pygame.mixer.music.set_endevent(MUSIC_EVENT)
        self.paused = False
        if musicList[0] == shelf['lastPlayed']:
            self.advanceMusicList()
        self.newSong()

    def advanceMusicList(self):
        global musicList
        # Put the first track at the end of the list.
        musicList = musicList[1:] + musicList[0:1]

    def newSong(self):
        music = pygame.mixer.music
        music.load(os.path.join('data', musicList[0]))
        music.play()

        shelf['lastPlayed'] = musicList[0]
        self.advanceMusicList()

    def toggleMusic(self):
        music = pygame.mixer.music
        if self.paused:
            music.unpause()
            self.paused = False
        else:
            music.pause()
            self.paused = True

    def load(self, name, ext="wav"):
        sound = pygame.mixer.Sound(os.path.join('data', name+'.'+ext))
        sound.set_volume(0.2)
        self.sounds[name] = sound

    def get(self, name):
        return self.sounds[name]

    def play(self, name, loops=0, maxtime=0):
        return self.sounds[name].play(loops, maxtime)

    def stop(self, name):
        self.sounds[name].stop()

def initMixer():
    global mixer
    mixer = Mixer()

postGLInits.append(initMixer)
    
# Circle - a simple entity for testing {{{1
class Circle(Entity):
    def __init__(self, radius, **msg):
        self.radius = radius

    def setRadius(self, radius, **msg):
        self.radius = radius

# Square - a drawable entity {{{1
class Square(Entity):
    def __init__(self, size, location, color, **msg):
        self.size = size
        self.location = location
        self.color = color

        scene.add(self, 0)

    def setSize(self, size, **msg):
        self.size = size

    def setLocation(self, location, **msg):
        self.location = location

    def setColor(self, color, **msg):
        self.color = color

    def draw(self):
        loc = self.location; size = self.size

        glColor3fv(self.color)
        glBegin(GL_QUADS)
        glVertex3f(loc[0]-size, loc[1]+size, loc[2])
        glVertex3f(loc[0]+size, loc[1]+size, loc[2])
        glVertex3f(loc[0]+size, loc[1]-size, loc[2])
        glVertex3f(loc[0]-size, loc[1]-size, loc[2])
        glEnd()

    def doneDrawing(self):
        return False

# Sprite {{{1
# Mode for drawing textures - set by init() below.
textureFlag = None   

# Try to avoid gl state changes by remembering what options are enabled
boundTexture = None
def lazyBindTexture(texture):
    global boundTexture
    if texture != boundTexture:
        glBindTexture(textureFlag, texture)
        glDisable(textureFlag)
        glEnable(textureFlag)
        boundTexture = texture

def nextPowerOfTwo(n):
    p=1
    while p<n:
        p *= 2
    return p

def padSprite(imageData, oldWidth, oldHeight, newWidth, newHeight):
    """Given imageData representing a sprite of a certain dimensions, add pixels
    consisting of RGBA(0,0,0,0) values to make data for a sprite of new
    dimensions.  The old sprite is in the bottom left corner of the new
    sprite."""
    assert oldWidth <= newWidth
    assert oldHeight <= newHeight
    assert len(imageData) == 4 * oldWidth * oldHeight

    newData = []
    topPad = '\x00' * (4 * newWidth)
    for i in range(oldHeight):
        newData.append(imageData[4 * oldWidth * i : 4 * oldWidth * (i+1)])
        rightPad = '\x00' * (4 * (newWidth-oldWidth))
        newData.append(rightPad)
    for i in range(oldHeight, newHeight):
        newData.append(topPad)
    return ''.join(newData)

class AbstractSprite:
    def __init__(self, name, numFrames=1, width=None, height=None, columns=1):
        """Create a sprite from an image file.  If the name of the sprite is
        'gandalf' then the file will be data/gandalf.png.  numFrames gives the
        number of frames of animation of the sprite.  width and height give the
        dimensions of each frame.  columns specifies the numbers of columns in
        which the sprite is layed out in the file, ie. if columns=4 then frames
        0-3 are in the top row of the image file, frame 4-7 are in the next row,
        etc."""
        image = pygame.image.load(os.path.join('data', name+'.png'))

        if width==None: width = image.get_width()
        if height==None: height = image.get_height()

        assert image.get_width() >= width * columns
        assert image.get_height() >= height * ((numFrames-1)/columns+1)
        
        self.name = name
        self.numFrames = numFrames
        self.width = width
        self.height = height
        self.columns = columns

        self.texture = glGenTextures(1)

        self.textureFromImage(image)

    def draw(self, frame, location, flipped=False):
        assert 0 <= frame and frame < self.numFrames
        lazyBindTexture(self.texture)
        
        (x,y) = location
        width = self.width; height = self.height
        u = (frame % self.columns) * width
        v = (frame / self.columns) * height

        if flipped:
            u0 = u + width; u1 = u
        else:
            u0 = u; u1 = u + width
        v0 = v+height; v1 = v

        assert glIsEnabled(textureFlag)

        glBegin(GL_QUADS)
        self.texCoord(u0, v0)
        glVertex2f(x, y)
        self.texCoord(u1, v0)
        glVertex2f(x+width, y)
        self.texCoord(u1, v1)
        glVertex2f(x+width, y+height)
        self.texCoord(u0, v1)
        glVertex2f(x, y+height)
        glEnd()


    def drawCentered(self, frame, flipped=False):
        self.draw(frame, (-self.width/2.0, -self.height/2.0), flipped)

    def drawPartial(self, frame, location, bbox, flipped=False):
        """This draws part of the sprite as determined by the bounding box,
        whose coordinates are given in the range [0,1].  For example,
        bbox=(0,0,1,0.5) will draw the bottom half of the sprite."""
        assert 0 <= frame and frame < self.numFrames
        lazyBindTexture(self.texture)
        
        (x,y) = location
        width = self.width; height = self.height
        u = (frame % self.columns) * width
        v = (frame / self.columns) * height

        (l,b,r,t) = bbox

        xmin = width * l; xmax = width * r
        ymin = height * b; ymax = height * t

        if flipped:
            u0 = u + width - xmin; u1 = u + width - xmax
        else:
            u0 = u + xmin; u1 = u + xmax
        v0 = v+height-ymin; v1 = v+height-ymax

        assert glIsEnabled(textureFlag)

        glBegin(GL_QUADS)
        self.texCoord(u0, v0)
        glVertex2f(x+xmin, y+ymin)
        self.texCoord(u1, v0)
        glVertex2f(x+xmax, y+ymin)
        self.texCoord(u1, v1)
        glVertex2f(x+xmax, y+ymax)
        self.texCoord(u0, v1)
        glVertex2f(x+xmin, y+ymax)
        glEnd()


class SquareSprite(AbstractSprite):
    def textureFromImage(self, image):
        imageData = pygame.image.tostring(image, "RGBA", False)

        self.size = nextPowerOfTwo(max(image.get_width(),image.get_height()))

        assert glIsEnabled(textureFlag)

        lazyBindTexture(self.texture)
        glTexParameteri(textureFlag,
                        GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(textureFlag,
                        GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexImage2D(textureFlag,
                     0, 
                     GL_RGBA,
                     self.size, self.size,
                     0,
                     GL_RGBA, GL_UNSIGNED_BYTE,
                     padSprite(imageData,
                               image.get_width(), image.get_height(),
                               self.size, self.size))


    def texCoord(self, u, v):
        glTexCoord2d(float(u)/float(self.size), float(v)/float(self.size))


class RectangularSprite(AbstractSprite):
    def textureFromImage(self, image):
        imageData = pygame.image.tostring(image, "RGBA", False)

        assert glIsEnabled(textureFlag)

        lazyBindTexture(self.texture)
        glTexParameteri(textureFlag,
                        GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(textureFlag,
                        GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexImage2D(textureFlag,
                     0, 
                     GL_RGBA,
                     image.get_width(), image.get_height(),
                     0,
                     GL_RGBA, GL_UNSIGNED_BYTE,
                     imageData)


    def texCoord(self, u, v):
        glTexCoord2d(u, v)

spriteClassByMode = { GL_TEXTURE_2D : SquareSprite,
                      GL_TEXTURE_RECTANGLE_ARB: RectangularSprite }

spriteNames = [
        'factory', 'plant', 'munitions', 'gateway', 'mutator',
        'gun', 'shield', 'shield_flipped',
        'upper', 'upper_shield', 'upper_shield_flipped',
        'girder',
        'missile', 'virus',
        'cloud', 'title' ]
class SpriteFactory:
    """This loads all of the sprites in at the start, so that they can be
    accessed by name."""
    def __init__(self):
        self.db = {}

        self.spriteClass = spriteClassByMode[textureFlag]

        for name in spriteNames:
            self.add(name)

        self.add('font', 256, 16, 16, 16)

    def add(self, name, numFrames=1, width=None, height=None, columns=1):
        self.db[name] = \
                self.spriteClass(name, numFrames, width, height, columns)

    def get(self, name):
        return self.db[name]

def initSprites():
    global sprites
    sprites = SpriteFactory()

postGLInits.append(initSprites)

# Star {{{1
class Star(Entity):
    """Simple test class for entities with sprites."""
    def __init__(self, location, **msg):
        self.location = location
        self.sprite = sprites.get('star')

    def draw(self):
        glColor4f(1,1,1,1)
        #self.sprite.draw(0, self.location)
        self.sprite.drawPartial(0, self.location, (0,0,1,0.5))
        glColor4f(1,1,1,0.5)
        self.sprite.drawPartial(0, self.location, (0,0.5,1,1))

    def doneDrawing(self):
        return False

# Label {{{1
def drawString(str, location, justification='center', size=None):
    sprite = sprites.get('font')
    if size == None:
        size = sprite.width
    if hasattr(str, '__iter__'):
        for s in str:
            drawString(s, location, justification, size)
            location = (location[0], location[1] - size)
    else:
        width = len(str)*size
        x,y = location
        if justification == 'right':
            x -= width
        elif justification in ['center', 'centre']:
            x -= width/2
        else:
            assert justification == 'left'

        for i in range(len(str)):
            frame = ord(str[i])-32
            sprite.draw(frame, (x+size*i, y))

class Label:
    def __init__(self, text, location, justification='center', size=None):
        self.text = text
        self.location = location
        assert justification in ['left', 'center', 'centre', 'right']
        self.justification = justification
        self.size = size
        self.done = False
        self.color = (1,1,1,0.5)

        self.show()

    def show(self):
        scene.add(self, LABEL_LAYER)
        self.shown = True
    def hide(self):
        scene.remove(self)
        self.shown = False
    def toggle(self):
        if self.shown:
            self.hide()
        else:
            self.show()

    def draw(self):
        glColor4fv(self.color)
        drawString(self.text, self.location, self.justification, self.size)

    def doneDrawing(self):
        return self.done

messageBar = Label([],
        (width/2, height-32), 'center')

clock = pygame.time.Clock()
fpsBox = Label('', (width-10,height-26), 'right')
fpsBox.hide()
def showFPS():
    fpsBox.text = ["FPS:%3.0f" % (clock.get_fps()),
                   "Objects:%4d" % len(scene.objs)]
    if 'right' in towers:
        girder = towers['right'].weakestGirder()
        if girder:
            fpsBox.text.append("Protection:%4d" % girder.protection())

def toggleFPS():
    fpsBox.toggle()

# Button {{{1
class Widget(object):
    def show(self):
        scene.add(self, LABEL_LAYER)
        clickables.add(self, self.bbox)

    def hide(self):
        scene.remove(self)
        clickables.remove(self)

class Button(Widget):
    def __init__(self, text, location, onClick):
        """A text button with onClick as a callback function."""
        self.font = sprites.get('font')
        self.border = 4

        self.location = location
        self.clickNoise = True
        self.onClick = onClick
        self.text = text
        self.resize()

        scene.add(self, LABEL_LAYER)

    def resize(self):
        x,y = self.location; border = self.border
        width = len(self.text)*self.font.width
        height = self.font.height

        self.bbox = (x - width/2 - border, y - border,
                     x + width/2 + border, y + height + border)

        clickables.add(self, self.bbox)

    def draw(self):
        if clickables.selected == self:
            # Clicked colors
            fgColor = (0.9, 0.9, 1.0, 0.5)
            bgColor = (0.0, 0.0, 0.0, 0.6)
        elif clickables.focus == self:
            # Mouse over colors
            fgColor = (1.0, 1.0, 1.0, 0.7)
            bgColor = (0.0, 0.0, 0.0, 0.5)
        else:
            # Normal colors
            fgColor = (1.0, 1.0, 0.9, 0.5)
            bgColor = (0.0, 0.0, 0.0, 0.2)

        drawRoundedRectangle(self.bbox, bgColor)

        glColor4fv(fgColor)
        drawString(self.text, self.location, 'center')

    def doneDrawing(self):
        return False

    def click(self, location):
        pass
    def hold(self, location, duration):
        pass
    def release(self, location):
        self.onClick()
        return True

    def hit(self, damage):
        pass

#def mom():
#    print "hi mom!"
#
#def initButton():
#    global button
#    button = Button("Say Hi", (width/2, 100), mom)
#
#postGLInits.append(initButton)

# TextBox {{{1
class TextBox(Widget):
    def __init__(self, location, capacity, text='', onEnter=None):
        """A text button with onClick as a callback function."""
        self.font = sprites.get('font')
        self.border = 4

        self.location = location
        self.capacity = capacity
        self.text = text
        self.onEnter = onEnter

        scene.add(self, LABEL_LAYER)

        x,y = self.location; border = self.border
        width = self.capacity*self.font.width
        height = self.font.height

        self.bbox = (x - width/2 - border, y - border,
                     x + width/2 + border, y + height + border)

        self.clickNoise = True
        clickables.add(self, self.bbox)

    def drawCarat(self, color):
        if pygame.time.get_ticks() / 500 % 2 == 0:
            x,y = self.location
            width = len(self.text)*self.font.width
            height = self.font.height

            drawRectangle((x + width/2, y, x + width/2 + 2, y + height), color)

    def drawBackground(self, bgColor):
        r,g,b,a = bgColor
        x,y = self.location
        width = len(self.text)*self.font.width
        height = self.font.height
        padding = 2*self.font.width
        border = self.border

        with NoTexture():
            glBegin(GL_QUAD_STRIP)
            glColor4f(r,g,b,0)
            glVertex2f(x - width/2 - padding, y - border)
            glVertex2f(x - width/2 - padding, y + height + border)
            glColor4f(r,g,b,a)
            glVertex2f(x - width/2, y - border)
            glVertex2f(x - width/2, y + height + border)
            glVertex2f(x + width/2, y - border)
            glVertex2f(x + width/2, y + height + border)
            glColor4f(r,g,b,0)
            glVertex2f(x + width/2 + padding, y - border)
            glVertex2f(x + width/2 + padding, y + height + border)
            glEnd()

    def draw(self):
        if clickables.selected == self:
            # Clicked colors
            fgColor = (1.0, 1.0, 1.0, 0.5)
            bgColor = (0.0, 0.0, 0.0, 0.6)
        elif clickables.focus == self:
            # Mouse over colors
            fgColor = (1.0, 1.0, 1.0, 0.7)
            bgColor = (0.0, 0.0, 0.0, 0.5)
        else:
            # Normal colors
            fgColor = (1.0, 1.0, 1.0, 0.5)
            bgColor = (0.0, 0.0, 0.0, 0.2)

        #drawRectangle(self.bbox, bgColor)
        self.drawBackground(bgColor)

        glColor4fv(fgColor)
        drawString(self.text, self.location, 'center')

        if clickables.selected == self:
            self.drawCarat(fgColor)

    def doneDrawing(self):
        return False

    def unselect(self):
        clickables.unselect()
        pygame.key.set_repeat()

    def click(self, location):
        if locationInBBox(location, self.bbox):
            pygame.key.set_repeat(500, 50)
        else:
            self.unselect()

    def hold(self, location, duration):
        pass
    def release(self, location):
        return False
    def key(self, unicode, key, mod):
        if key == K_RETURN:
            self.unselect()
            if self.onEnter:
                self.onEnter(self.text)
            return True
        elif key in [K_BACKSPACE, K_DELETE]:
            if len(self.text) > 0:
                self.text = self.text[:-1]
            return True
        else:
            if len(self.text) < self.capacity:
                self.text += unicode
            return True

    def hit(self, damage):
        self.text = "Don't hit me!"

#def entered(text):
#    print "entered:", text
#def initTextBox():
#    global boxers
#    boxers = TextBox((width/2, 150), 40, 'TextBoxers', entered)
#postGLInits.append(initTextBox)

# Title {{{1
class Title:
    def __init__(self, location, color):
        self.location = location
        self.color = color
        self.sprite = sprites.get('title')

        self.show()

    def show(self):
        scene.add(self, LABEL_LAYER)
    def hide(self):
        scene.remove(self)

    def draw(self):
        glColor4fv(self.color)
        with PushedMatrix():
            glTranslatef(self.location[0], self.location[1], 0.0)
            self.sprite.drawCentered(0)
    
    def doneDrawing(self):
        return False

def initTitle():
    global title
    title = Title((width/2, height - 70), (1,1,1,0.6))
postGLInits.append(initTitle)

# Backdrop {{{1
class Backdrop:
    def draw(self):
        if updater.paused:
            top = (0.22, 0.24, 0.38); bottom = (0.27, 0.40, 0.51)
        elif victor:
            top = (0.75, 0.13, 0.01); bottom = (0.98, 0.51 , 0.19)
        else:
            #top = (0.02, 0.09, 0.56); bottom = (0.0, 0.42, 0.77)
            #top = (0.10, 0.20, 0.66); bottom = (0.0, 0.42, 0.77)
            top = (0.05, 0.15, 0.66); bottom = (0.0, 0.42, 0.77)
        with NoTexture():
            glBegin(GL_QUADS)
            glColor3fv(bottom)
            glVertex2i(0,0); glVertex2i(width,0)
            glColor3fv(top)
            glVertex2i(width,height); glVertex2i(0, height)
            glEnd()

    def doneDrawing(self):
        return False

scene.add(Backdrop(), BACKDROP_LAYER)

# Cube {{{1
class Cube:
    """Test for the perspective code."""
    def draw(self):
        with Perspective((width/2, height/2)):
            with NoTexture():
                glColor3f(1,1,1)
                with glGroup(GL_LINE_LOOP):
                    glVertex3f(width/3,height/3,2)
                    glVertex3f(2*width/3,height/3,2)
                    glVertex3f(2*width/3,height/3,4)
                    glVertex3f(width/3,height/3,4)

    def doneDrawing(self):
        return False

#scene.add(Cube(), -50)
# TargetManager {{{1
class TargetManager:
    """This translates action of the mouse to selections of object in the level.
    Each entity has to register itself with a bounding box (left, bottom, right,
    top) to the target manager.  Then, when the bounding box is clicked, the
    click method of the entity is called."""
    def __init__(self):
        self.objs = {}
        self.focus = None
        self.selected = None
        self.pressed = False
        self.lastTicks = None

    def add(self, obj, bbox):
        self.objs[obj] = bbox

    def remove(self, obj):
        if self.selected == obj:
            self.unselect()
        if self.focus == obj:
            self.focus = None
        if obj in self.objs:
            del self.objs[obj]

    def minimalBBoxes(self, pairs):
        if len(pairs) <= 1:
            return pairs

        minimal = []

        def isMinimal(pair):
            for m in minimal:
                if bboxInBBox(m[1], pair[1]):
                    return False
            return True
        def addMinimal(pair):
            for i in range(len(minimal)):
                if bboxInBBox(pair[1], minimal[i][1]):
                    del minimal[i]
            minimal.append(pair)

        for pair in pairs:
            if isMinimal(pair):
                addMinimal(pair)

        return minimal

    def closestObject(self, location, pairs):
        if len(pairs) == 0:
            return None
        if len(pairs) == 1:
            return pairs[0][0]

        def dist(location, pair):
            return dist2(location, bboxCenter(pair[1]))

        best = pairs[0]; bestDist = dist(location, pairs[0])
        for pair in pairs[1:]:
            distance = dist(location, pair)
            if distance < bestDist:
                best = pair; bestDist = distance
        return best[0]

    def objectAtLocation(self, location):
        candidates = [pair for pair in self.objs.items() \
                                    if locationInBBox(location, pair[1]) ]
        mins = self.minimalBBoxes(candidates)
        winner = self.closestObject(location, mins)
        return winner

    def updateMessageBar(self):
        if self.focus and hasattr(self.focus, 'desc'):
            messageBar.text = [self.focus.__class__.__name__, self.focus.desc()]
        else:
            messageBar.text = []

    def focusOnLocation(self, location):
        oldFocus = self.focus
        self.focus = self.objectAtLocation(location)
        self.updateMessageBar()

        if self.focus != None and self.focus != oldFocus and \
                hasattr(self.focus, 'clickNoise'):
            mixer.play('click')

    def unselect(self):
        self.focus = None
        self.selected = None

    def click(self, location, ticks):
        assert not self.pressed
        self.pressed = True
        self.lastTicks = ticks

        if not updater.paused:
            if self.selected:
                self.selected.click(location)
            else:
                self.focusOnLocation(location)
                if self.focus:
                    self.selected = self.focus
                    self.selected.click(location)

    def drag(self, location, ticks, minDuration = 50):
        if not updater.paused:
            duration = ticks - self.lastTicks
            if self.selected and duration >= minDuration:
                # Give the duration of the hold to the object.
                self.selected.hold(location, duration)
                self.lastTicks = ticks

    def move(self, location, ticks):
        if self.pressed:
            self.drag(location, ticks)
        elif self.selected == None:
            self.focusOnLocation(location)

    def release(self, location, ticks):
        """Tell the selected object that the mouse is released.  If the object's
        release method returns true, it means that the object is done being
        selected.  Otherwise, it will continue to receive new clicks."""
        assert self.pressed
        self.pressed = False
        
        # First finish the holding
        self.drag(location, ticks, minDuration = 0)

        if self.selected:
            if self.selected.release(location):
                self.selected = None
                self.focusOnLocation(location)


    def key(self, unicode, key, mod):
        """Receive a key event."""
        if self.selected and hasattr(self.selected, 'key'):
            return self.selected.key(unicode, key, mod)
        else:
            return False

    def update(self, ticks, duration):
        if self.pressed:
            self.drag(mousePos(), ticks)

    def hit(self, location, radius, damage):
        for obj, bbox in self.objs.copy().iteritems():
            dist = distToBBox(location, bbox)
            if dist < radius:
                factor = (1.0 - float(dist) / float(radius))**2
                #print "hit", obj.model, "for damage", factor*damage
                obj.hit(factor*damage)
                
clickables = TargetManager()
updater.add(clickables, 0)

targets = TargetManager()
#updater.add(targets, 0)


# Victor {{{1
victor = None

class VictoryMessage(Entity):
    """Create this entity to send out news that one of the sides has one."""
    def __init__(self, side, **msg):
        global victor
        if victor == None:
            victor = side
            Label('The '+side+' is victorious!', (width/2, height/2), 'center')

# Building {{{1
class Building(Entity):
    """This is the base class for a building, an object that is built by holding
    the mouse down on its blueprint, and once built can be used for various
    purposes."""

    def __init__(self, name, model, side, bbox, **msg):
        self.name = name
        self.model = model
        self.side = side
        self.bbox = bbox

        self.state = 'blueprint'
        self.stats = buildingStats[model]

        self.hp = 0
        if 'location' not in self.__dict__:
            self.location = (bbox[0], bbox[1])

        self.buildPressed = False
        self.rubbleTime = None

        self.alternate = None
        self.show()

        self.persistentSoundChannel = None

    def desc(self):
        if 'desc' in self.stats:
            return self.stats['desc']
        else:
            return ''

    def mouseOver(self):
        return clickables.focus == self

    def active(self):
        """Returns True if the building is finished construction and can be
        used."""
        return self.state == 'built'

    def color(self):
        if self.state == 'blueprint':
            if self.mouseOver():
                return buildingColor
            elif self.playing():
                return blueprintColor
            else:
                return None
        elif self.state in ['rubble', 'floating']:
            t = updater.get_ticks() - self.rubbleTime
            if t < rubbleDuration:
                if self.state == 'rubble' and self.playing():
                    endColor = blueprintColor
                else:
                    endColor = (0,0,0,0)
                return interp(float(t)/float(rubbleDuration),
                              rubbleColor, endColor)
            elif self.state == 'rubble' and self.playing():
                return blueprintColor
            else:
                # Building no longer exists, don't draw.
                return None
        elif self.state == 'building':
            return None
        else:
            if self.state != 'built':
                print 'state:', self.state
                assert self.state == 'built'
            return interp(float(self.hp)/float(self.maxhp()),
                          damagedColor, builtColor)

    def playPersistentSound(self, name, loops=0, maxtime=0):
        """Play a sound that runs continuously, such as the construction sound,
        or shield charging.  This routine ensures that such sounds don't
        continue after the building is destroyed."""
        self.stopPersistentSound()
        self.persistentSoundChannel = mixer.play(name, loops=-1)

    def stopPersistentSound(self):
        if self.persistentSoundChannel:
            self.persistentSoundChannel.stop()
            self.persistentSoundChannel = None

    def maxhp(self):
        return self.stats['hp']

    def percentComplete(self):
        return float(self.hp) / float(self.maxhp())

    def complete(self, sound='built', **msg):
        """Put into a completed state."""
        self.hp = self.maxhp()
        self.state = 'built'
        if sound:
            mixer.play(sound)

        if self.local():
            if 'towers' in globals():
                self.tower().audit()
            if self.alternate:
                self.alternate.broadcast('hide')


    def hit(self, damage):
        newhp = max(self.hp - damage, 0.0)
        if newhp == 0.0 and self.state == 'built':
            self.broadcast('destroy')
        else:
            self.broadcast('damage', newhp = newhp)

    def infect(self, level):
        if self.state == 'built':
            if self.tower().infect(level):
                self.broadcast('destroy')
        
    def destroy(self, **msg):
        self.hp = 0
        self.state = 'rubble'
        self.rubbleTime = updater.get_ticks()

        # Stop any sound (shield charging, gun loading, etc.) that was playing
        # when the building was destroyed.
        self.stopPersistentSound()

        mixer.play(random.choice(destroyList))

        if self.local():
            if 'towers' in globals() and self.local():
                self.tower().audit()
            if self.alternate:
                self.alternate.show()

    def float(self, **msg):
        """The level supporting this building has been destroyed.  Draw a death
        sequence for the building, then eliminate it."""
        if self.active():
            self.hp = 0
            self.state = 'floating'
            self.rubbleTime = updater.get_ticks()
        else:
            self.state = 'blueprint'
            scene.remove(self)

        if self.local():
            updater.remove(self)
            targets.remove(self)
        if self.playing():
            clickables.remove(self)

    def hide(self, **msg):
        assert not self.active()
        self.shown = False
        scene.remove(self)
        if self.local():
            updater.remove(self)
            targets.remove(self)
        if self.playing():
            clickables.remove(self)

    def show(self, **msg):
        assert not self.active()
        if self.state != 'floating':
            self.shown = True
            scene.add(self, BUILDING_LAYER)
            if self.local():
                updater.add(self, 0)
                targets.add(self, self.bbox)
            if self.playing():
                clickables.add(self, self.bbox)

    def done(self):
        """Floating buildings no longer exist after their animation finishes."""
        if self.state == 'floating':
            return updater.get_ticks() > self.rubbleTime + rubbleDuration
        else:
            return False

    def damage(self, newhp, **msg):
        self.hp = newhp

    def activeUpdate(self, ticks, duration):
        pass
    def activeClick(self, location):
        pass
    def activeHold(self, location, duration):
        pass
    def activeRelease(self, location):
        return True

    def update(self, ticks, duration):
        # Neglected buildings under construction slowly decline.
        if self.active():
            self.activeUpdate(ticks, duration)
        elif self.state == 'building' and not self.buildPressed:
            maxhp = self.stats['hp']
            self.hp -= disrepairFactor * float(maxhp) * float(duration) / \
                       float(self.stats['buildtime'])
            if self.hp < 0:
                self.hp = 0
                self.state = 'blueprint'
        elif self.state in 'rubble':
            t = updater.get_ticks() - self.rubbleTime
            if t > rubbleDuration:
                self.state = 'blueprint'
                self.deathTime = None
        elif self.done():
            updater.remove(self)
            targets.remove(self)
            clickables.remove(self)

    def click(self, location):
        if self.active():
            self.activeClick(location)
        else:
            if self.state == 'blueprint':
                self.state = 'building'

            if not self.buildPressed:
                if self.playing():
                    self.playPersistentSound('building')
                self.buildPressed = True

    def hold(self, location, duration):
        if not self.buildPressed and self.active():
            self.activeHold(location, duration)
        elif self.state == 'building':
            maxhp = self.stats['hp']
            self.hp += self.tower().factoryBoost() * \
                       self.tower().handicap * \
                       float(maxhp) * float(duration) / \
                       float(self.stats['buildtime'])
            if self.hp >= maxhp:
                if self.playing():
                    self.stopPersistentSound()
                self.broadcast('complete')

    def release(self, location):
        if not self.buildPressed and self.active():
            return self.activeRelease(location)
        else:
            self.buildPressed = False
            if self.playing():
                self.stopPersistentSound()
            return True

    def doneDrawing(self):
        return self.done()


# SpriteBuilding {{{1
class SpriteBuilding(Building):
    def __init__(self, name, model, side, location, **msg):
        self.sprite = sprites.get(model)

        x,y = location
        bbox = (x, y, x+self.sprite.width, y+self.sprite.height)

        Building.__init__(self, name, model, side, bbox)

    def destroy(self, **msg):
        createDebris(self.bbox)
        Building.destroy(self, **msg)


    def draw(self):
        if self.state == 'building':
            percent = self.percentComplete()
            glColor4fv(builtColor)
            self.sprite.drawPartial(0, self.location, (0,0,1,percent))
            glColor4fv(buildingColor)
            self.sprite.drawPartial(0, self.location, (0,percent,1,1))
        else:
            color = self.color()
            if color != None:
                glColor4fv(color)
                self.sprite.draw(0, self.location)

def buildingWidth(building):
    building = nameof(building).lower()
    return sprites.get(building).width

# Some self-indulgent metaprogramming.
def addBuildingModel(model):
    # Capitalize name.
    model = model.lower()
    Model = model[0].upper() + model[1:]
    class XBuilding(SpriteBuilding):
        def __init__(self, name, side, location, **msg):
            SpriteBuilding.__init__(self, name, model, side, location, **msg)
    XBuilding.__name__ = Model
    globals()[Model] = XBuilding

for model in ['factory', 'munitions']:
    addBuildingModel(model)

# Plant {{{1
class Plant(SpriteBuilding):
    def __init__(self, name, side, location, **msg):
        SpriteBuilding.__init__(self, name, 'plant', side, location, **msg)

    def complete(self, **msg):
        SpriteBuilding.complete(self, **msg)
        if self.local():
            self.tower().reshield()
    def destroy(self, **msg):
        SpriteBuilding.destroy(self, **msg)
        if self.local():
            self.tower().reshield()
    def float(self, **msg):
        SpriteBuilding.float(self, **msg)
        if self.local():
            self.tower().reshield()

# Missile {{{1

def radians(angle): return angle * math.pi / 180.0
def degrees(angle): return angle / math.pi * 180.0

def maxSpeedFromRange(range):
    """Determine the minimum velocity needed just to reach the target."""
    dx, dy = range

    if dx == 0.0:
        assert dy > 0.0
        return sqrt(2.0 * gravity * dy)

    l = length(range)

    # Through the magic of Lagrange multipliers, we figure out the slope has to
    # be:
    # slope = (l + dy) / dx
    # Then using the vxAtAngle formulae:
    v2 = 0.5 * gravity * (dx**2 + (l + dy)**2) / l
    assert v2 > 0.0

    return sqrt(v2)

def vxAtAngle(dx, dy, angle):
    """Calculate vx, the horizontal component of the velocity, if the missile is
    restricted to fire at the given angle.  Returns None if it is impossible at
    that angle."""
    if dx == 0.0:
        return None
    denom = tan(radians(angle)) * abs(dx) - dy
    if denom <= 0.0:
        return None

    vx2 = 0.5 * (gravity * dx**2) / denom
    assert vx2 > 0.0

    if dx >= 0.0:
        return sqrt(vx2)
    else:
        return -sqrt(vx2)

def trajectoryTimes(dx, dy, speed):
    a = gravity**2 / 4.0
    b = dy * gravity - speed**2
    c = dx**2 + dy**2

    solns = solveQuadratic(a,b,c)
    return [sqrt(soln) for soln in solns if soln > 0.0]

def trajectoryVelocity(dx, dy, speed, T):
    assert T > 0
    vx = dx/T
    vy = sqrt(speed**2 - vx**2)
    return (vx, vy)

def velocityTimeOfMissile(source, target, maxspeed, minAngle):
    dx = target[0] - source[0]
    dy = target[1] - source[1]

    # First try to fire at the minimum angle.
    vx = vxAtAngle(dx, dy, minAngle)
    if vx != None:
        v = (vx, abs(vx) * tan(radians(minAngle)))
        if length(v) <= maxspeed:
            return v, dx/vx

    times = trajectoryTimes(dx, dy, maxspeed)
    for T in times:
        v = trajectoryVelocity(dx, dy, maxspeed, T)
        angle = atan2(v[1], abs(v[0]))
        if degrees(angle) >= minAngle:
            return v, T
    return None, None

def dudVelocity(source, target, speed):
    dx = target[0] - source[0]
    dy = target[1] - source[1]

    rise = dy + length((dx, dy)); run = dx; hyp = length((rise,run))
    return (run/hyp * speed, rise/hyp * speed)

def dudTime(source, target, velocity):
    dx = target[0] - source[0]
    dy = target[1] - source[1]
    vx, vy = velocity

    if vx == 0.0:
        return max(vy / gravity, 0.0)

    # Calculate the time for the missile to be directly under the target. Then
    # use a linear approximation to the trajectory at that point, and move to
    # the closest point on that line.
    t = dx / vx

    y = - 0.5 * gravity * t**2 + vy * t
    vyt = - gravity * t + vy
    assert y < dy
    y_error = dy - y

    x_error = y_error / (vyt/vx + vx/vyt)

    return t + x_error / vx


class Missile(Entity):
    def __init__(self, side, gun, source, target, stats, **msg):
        """Launch a missile from the source location to the target location.
        The stats field gives yield, """
        self.side = side
        self.sprite = sprites.get('missile')

        self.gun = gun
        self.source = source; self.target = target
        self.stats = stats

        maxSpeed = maxSpeedFromRange(stats['range'])
        minAngle = stats['angle']
        v, t = velocityTimeOfMissile(source, target, maxSpeed, minAngle)
        if v == None:
            v = dudVelocity(source, target, maxSpeed)
            t = dudTime(source, target, v)

        acc = stats['accuracy']
        self.startVelocity = (v[0] * random.gauss(1.0, acc),
                              v[1] * random.gauss(1.0, acc))
        self.time = t

#        print "maxspeed:", maxspeed, "speed:", length(self.startVelocity)
#        print "startAngle:", \
#                atan2(self.startVelocity[1], self.startVelocity[0]) * 180.0 / \
#                math.pi

        self.startTicks = updater.get_ticks()
        self.exploded = False
        
        # Add a contrail (stream of cloud in a line), a flame off of the back of
        # the missile, and a cloud of smoke on the launch pad.
        if graphicsLevel() > 1:
            Contrail(self)
            createPlume((self.source[0], self.source[1]-self.sprite.height))
        Flame(self)

        scene.add(self, MISSILE_LAYER)
        if self.local():
            updater.add(self, 0)
            entities[gun].broadcast('fire')

        mixer.play('missile')

    def position(self, t):
        x = self.source[0] + self.startVelocity[0] * t
        y = self.source[1] + self.startVelocity[1] * t - 0.5 * gravity * t**2
        return (x,y)

    def velocity(self, t):
        vx = self.startVelocity[0]
        vy = self.startVelocity[1] - gravity * t
        return (vx, vy)

    def angle(self, t):
        vx, vy = self.velocity(t)
        return atan2(vy, vx) * 180.0 / math.pi

    def explode(self, time, **msg):
        self.time = time
        position = self.position(time)
        vx, vy = self.velocity(time)
        mixer.play(random.choice(explodeList))
        for i in range(graphicsDependent(10, 35, 70)):
            Cloud('fire', position,
                  (random.gauss(0.05*vx, 0.02), random.gauss(0.05*vy+lift,0.02)))
        for i in range(graphicsDependent(5, 10, 15)):
            Cloud('smoke', position,
                  (random.gauss(0.05*vx, 0.02), random.gauss(0.05*vy+lift,0.02)))

    def detonate(self, time):
        self.broadcast('explode', time = time)
        self.exploded = True
        
        # Cause damage.
        targets.hit(self.position(time),
                    self.stats['radius'],
                    self.stats['damage'])

    def detonateOnShield(self, shield, time):
        p = self.position(time); v = self.velocity(time)
        q = shield.center(); r = shield.radius()

        disp = (p[0] - q[0], p[1] - q[1])

        # Use a linear approximation of the time, and solve for the time when it
        # first hits the shield.
        solns = solveQuadratic(length2(v),
                               2 * dot(disp,v),
                               length2(disp) - r**2)
        if solns == []:
            t = time
        else:
            t = solns[0]

        self.detonate(time + solns[0])
        shield.absorb(self.stats['damage'])

    def update(self, ticks, duration):
        if not self.exploded:
            t = ticks - self.startTicks
            shield = self.tower().shieldingAtLocation(self.position(t))
            if shield:
                self.detonateOnShield(shield, t)
            elif t >= self.time:
                self.detonate(self.time)
                self.tower().hitShield(self.position(self.time),
                                       self.stats['radius'],
                                       self.stats['damage'])

    def draw(self):
        # TODO: send ticks with draw command
        t = updater.get_ticks() - self.startTicks

        if t < self.time:
            with PushedMatrix():
                x,y = self.position(t)
                glTranslatef(x, y, 0)
                glRotatef(self.angle(t)-90.0, 0, 0, 1)
                scale = self.stats['scale']
                glScalef(scale, scale, scale)
                glColor4fv(missileColor)
                self.sprite.draw(0,
                        (-self.sprite.width/2,-self.sprite.height/2))
            #glPopMatrix()

    def doneDrawing(self):
        t = updater.get_ticks() - self.startTicks
        return t > self.time

    def vertices(self, t, width, offset=(0,0)):
        """Compute three vertices centered at the point the missile is at time t
        and in a line perpendicular to the missile with a width of width."""
        x,y = self.position(t)
        vx,vy = unit(self.velocity(t))
        w = float(width) / 2.0

        # Rotate 90 degrees.
        ux = vy; uy = -vx
        
        x += offset[0]; y += offset[1]

        return [(x-w*ux,y-w*uy), (x,y), (x+w*ux, y+w*uy)]

    def drawFlame(self):
        t0 = updater.get_ticks() - self.startTicks

        segments = 10; flameTime = 200
        flameColor = (1.0, 0.5, 0.0, 0.75)
        flameWidth = 8

        if t0 < self.time:
            glDisable(textureFlag)
            glColor4fv(flameColor)
            glBegin(GL_QUAD_STRIP)
            for i in range(segments):
                r = float(i) / float(segments)
                t = t0 - flameTime * r
                verts = self.vertices(t, (1.0 - r**2)*flameWidth,
                        (0,lift*(t0-t)))
                
                glVertex2fv(verts[0])
                glVertex2fv(verts[2])
            glEnd()
            glEnable(textureFlag)


    def drawContrail(self):
        T = updater.get_ticks() - self.startTicks
        t0 = min(T, self.time)

        duration = 10000
        if T - t0 > duration:
            return

        segments = graphicsDependent(10, 17, 20)
        startAlpha = 0.1
        endAlpha = 0.0

        startWidth = 12
        endWidth = 80

        with NoTexture():
            glBegin(GL_QUAD_STRIP)
            for i in range(segments):
                r = float(i) / float(segments-1)

                t = t0 * r

                # c=0 corresponds to a fresh part of the contrail
                # c=1 corresponds to a part at the end of its life
                c = min(float(T - t) / float(duration), 1.0)

                verts = self.vertices(t, interp(c, startWidth, endWidth),
                                      (0, lift*(T-t)))
                
                if i == 0 or i == segments - 1:
                    midAlpha = 0.0
                else:
                    midAlpha = interp(c, startAlpha, endAlpha)

                glColor4f(1,1,1,0)
                glVertex2fv(verts[0])
                glColor4f(1,1,1, midAlpha)
                glVertex2fv(verts[1])
            glEnd()
            glBegin(GL_QUAD_STRIP)
            for i in range(segments):
                r = float(i) / float(segments-1)
                t = t0 * r
                c = min(float(T - t) / float(duration), 1.0)

                verts = self.vertices(t, interp(c, startWidth, endWidth),
                                      (0, lift*(T-t)))
                
                if i == 0 or i == segments - 1:
                    midAlpha = 0.0
                else:
                    midAlpha = interp(c, startAlpha, endAlpha)

                glColor4f(1,1,1,midAlpha)
                glVertex2fv(verts[1])
                glColor4f(1,1,1,0)
                glVertex2fv(verts[2])
            glEnd()

    def doneDrawingContrail(self):
        T = updater.get_ticks() - self.startTicks
        t0 = min(T, self.time)

        duration = 10000
        return T - t0 > duration


        
class Contrail():
    def __init__(self, missile):
        self.missile = missile
        scene.add(self, CONTRAIL_LAYER)

    def draw(self):
        self.missile.drawContrail()

    def doneDrawing(self):
        return self.missile.doneDrawingContrail()

class Flame():
    def __init__(self, missile):
        self.missile = missile
        scene.add(self, FLAME_LAYER)

    def draw(self):
        self.missile.drawFlame()

    def doneDrawing(self):
        return self.missile.doneDrawing()


# Cloud {{{1

clouds = { 'smoke': { 'duration': [2500, 6000, 8000], 'offset': 500,
                      'startScale': 1.0, 'endScale': 5.0,
                      'color': (1,1,1, 0.2),
                      'colorVariance': (0.1, 0.1, 0.1, 0.1) },
           'fire':  { 'duration': [1000, 3000, 4000], 'offset': 100,
                      'startScale': 0.5, 'endScale': 3.0,
                      'color': (1.0, 0.5, 0.0, 0.6),
                      'colorVariance': (0.1, 0.1, 0.1, 0.1) },
           'dust':  { 'duration': [2000, 4000, 4000], 'offset': 1000,
                      'startScale': 2.0, 'endScale': 10.0,
                      'color': (0.8, 0.8, 0.8, 0.4),
                      'colorVariance': (0.0, 0.0, 0.0, 0.1) }}
class Cloud:
    def __init__(self, model, position, velocity):
        self.stats = clouds[model]
        self.position = position
        self.velocity = velocity
        self.angle = random.uniform(-math.pi,math.pi)
        self.startTicks = updater.get_ticks()
        self.sprite = sprites.get('cloud')

        r,g,b,a = self.stats['color']
        rv,gv,bv,av = self.stats['colorVariance']
        self.color = (random.gauss(r,rv),
                      random.gauss(g,gv),
                      random.gauss(b,bv),
                      random.gauss(a,av))

        scene.add(self, CLOUD_LAYER)

    def draw(self):
        t = updater.get_ticks() - self.startTicks + self.stats['offset']
        duration = graphicsDependent(*self.stats['duration'])
        if t > duration:
            return

        startScale = self.stats['startScale']
        endScale = self.stats['endScale']

        c = float(t) / float(duration)

        x = self.position[0] + t * self.velocity[0] * exp(-c)
        y = self.position[1] + t * self.velocity[1]

        scale = interp(c, startScale, endScale)
        
        r,g,b,a = self.color
        glColor4f(r, g, b, a * (1.0 - c)**2)
        with PushedMatrix():
            glTranslatef(x, y, 0)
            glRotatef(self.angle, 0,0,1)
            glScalef(scale, scale, scale)
            self.sprite.draw(0, (-self.sprite.width/2, -self.sprite.height/2))

    def doneDrawing(self):
        t = updater.get_ticks() - self.startTicks + self.stats['offset']
        duration = graphicsDependent(*self.stats['duration'])
        return t > duration

def createPlume(location, model = 'smoke'):
    for i in range(graphicsDependent(0, 8, 10)):
        Cloud(model, location,
              (random.gauss(0, 0.02), random.gauss(lift,0.004)))
    for i in range(graphicsDependent(0, 3, 3)):
        Cloud(model, location,
              (random.gauss(0, 0.02), random.gauss(lift,0.02)))

def createDebris(bbox, model = 'dust'):
    l,b,r,t = bbox
    for i in range(graphicsDependent(0, 10, 10)):
        Cloud(model, (random.uniform(l,r), random.uniform(b,t)),
              (random.gauss(0, 0.02), random.gauss(0,0.002)))

# Mount {{{1
class Mount(SpriteBuilding):
    def __init__(self, name, model, side, location, upper, **msg):
        SpriteBuilding.__init__(self, name, model, side, location, **msg)

        self.upper = upper
        if upper:
            self.upperSprite = sprites.get('upper')

    def complete(self, **msg):
        SpriteBuilding.complete(self)
        if self.upper:
            x,y = self.location
            self.bbox = (x, y - self.upperSprite.height,
                         x + self.sprite.width, y + self.sprite.height)
            if self.local():
                targets.add(self, self.bbox)
            if self.playing():
                clickables.add(self, self.bbox)

    def destroy(self, **msg):
        if self.upper:
            x,y = self.location
            self.bbox = (x, y,
                         x + self.sprite.width, y + self.sprite.height)
            if self.local():
                targets.add(self, self.bbox)
            if self.playing():
                clickables.add(self, self.bbox)
        SpriteBuilding.destroy(self)

    def draw(self):
        SpriteBuilding.draw(self)

        x,y = self.location
        if self.upper and self.state not in ['building', 'blueprint']:
            color = self.color()
            if color != None:
                glColor4fv(color)
                self.upperSprite.draw(0, (x, y-self.upperSprite.height))

# Gun {{{1
class Gun(Mount):
    def __init__(self, name, side, location, upper, **msg):
        Mount.__init__(self, name, 'gun', side, location, upper)

        self.missileSprite = sprites.get('missile')

        self.gunState = 'loading'
        self.heat = 0.0
        self.firePressed = False
        self.loadPercent = 0.0
        self.scale = 1.0

    def desc(self):
        if self.gunState == 'aiming':
            return "Click on target to fire."
        else:
            return Mount.desc(self)

    def missileStats(self):
        return self.tower().missileStats()

    def color(self):
        if self.active():
            return interp(self.heat, Mount.color(self), hotColor)
        else:
            return Mount.color(self)

    def resize(self):
        """Compute a bbox that will include the loading missile."""
        x,y = self.location
        missileHeight = self.missileSprite.height * self.loadPercent
        if self.upper and self.active():
            upperHeight = self.upperSprite.height
        else:
            upperHeight = 0.0

        self.bbox = (x,
                     y - upperHeight,
                     x + self.sprite.width,
                     y + self.sprite.height*self.scale + missileHeight)

        if self.local():
            targets.add(self, self.bbox)
        if self.playing():
            clickables.add(self, self.bbox)

    def draw(self):
        Mount.draw(self)

        x,y = self.location
        xoffset = float(self.sprite.width) / 2.0

        if self.active():
                if self.gunState == 'loading':
                    t = 1.0 - self.loadPercent
                    color = buildingColor
                elif self.gunState in ['loaded', 'aiming']:
                    t = 0.0
                    color = missileColor
                else:
                    return

                glColor4fv(color)
                with PushedMatrix():
                    glTranslatef(x + xoffset, y + self.sprite.height, 0.0)
                    scale = self.scale
                    glScalef(scale, scale, scale)
                    self.missileSprite.drawPartial(0,
                            (-self.missileSprite.width/2,
                             -t*self.missileSprite.height),
                            (0,t,1,1))

    def complete(self, **msg):
        Mount.complete(self)
        self.heat = 0.0
        self.loadPercent = 0.0
        self.resize()

        # Start loading a missile as soon as construction is complete.
        self.buildPressed = False
        if self.local():
            self.playPersistentSound('loading')

    def destroy(self, **msg):
        if self.gunState == 'aiming' and self.playing():
            pygame.mouse.set_cursor(*pygame.cursors.arrow)
            clickables.unselect()
        self.heat = 0.0
        self.loadPercent = 0.0
        self.resize()
        self.gunState = 'loading'

        Mount.destroy(self)

    def load(self, percent, scale, **msg):
        self.loadPercent = percent
        self.scale = scale
        self.resize()
        if self.loadPercent >= 1.0:
            self.loadPercent = 1.0
            self.gunState = 'loaded'
            if self.local():
                self.stopPersistentSound()
                mixer.play('loaded')

    def fire(self, **msg):
        self.gunState = 'loading'
        self.loadPercent = 0.0
        self.resize()

        if self.local():
            self.heat += self.missileStats()['heat']

    def activeUpdate(self, ticks, duration):
        if self.heat >= 1.0:
            self.broadcast('destroy')
        self.heat -= float(duration) / float(coolDownTime)
        if self.heat < 0.0:
            self.heat = 0.0

    def activeClick(self, location):
        if self.gunState == 'aiming':
            if sideOfScreen(location) == self.side:
                # Don't fire on yourself.
                mixer.play('nogo')
            else:
                self.firePressed = True
                if self.playing():
                    pygame.mouse.set_cursor(*pygame.cursors.arrow)
                source = (self.location[0]+float(self.sprite.width)/2,
                          self.location[1]+self.sprite.height
                                          +float(self.missileSprite.height)/2)
                createEntity(Missile,
                        side = otherSide(self.side),
                        gun = self.name,
                        source = source, # TODO: adjust for angle?
                        target = location,
                        stats = self.missileStats())
                # The created entity will send a 'fire' message back to this gun
                # when it is initialized.
        elif self.gunState == 'loading':
            self.playPersistentSound('loading')

    def activeHold(self, location, duration):
        gunLoadTime = self.missileStats()['loadTime']
        if self.gunState == 'loading' and not self.firePressed:
            newPercent = self.loadPercent + float(duration) / gunLoadTime
            self.broadcast('load', percent = newPercent,
                                   scale = self.missileStats()['scale'])

    def release(self, location):
        if self.firePressed:
            self.firePressed = False
        return Mount.release(self, location)

    def activeRelease(self, location):
        if self.gunState == 'loaded':
            self.gunState = 'aiming'
            if self.playing():
                pygame.mouse.set_cursor(*pygame.cursors.broken_x)
            clickables.updateMessageBar()
            return False
        elif self.gunState == 'loading':
            self.stopPersistentSound()
            return True
        else:
            assert self.gunState == 'aiming'
            return self.firePressed

# Shield {{{1
class Aura():
    def __init__(self, shield):
        self.shield = shield
        scene.add(self, AURA_LAYER)

    def draw(self):
        self.shield.drawAura()

    def doneDrawing(self):
        return self.shield.doneDrawing()

shieldAngle = radians(40.0)
shieldAngleSpan = radians(150.0)

class Shield(Mount):
    def __init__(self, name, side, location, upper, **msg):
        Mount.__init__(self, name, 'shield', side, location, upper)

        sourceOffset = (29,30)
        if side == 'right':
            self.sprite = sprites.get('shield_flipped')
            self.upperSprite = sprites.get('upper_shield_flipped')
            self.source = \
                    (self.location[0] + self.sprite.width - sourceOffset[0],
                     self.location[1] + sourceOffset[1])
        else:
            self.upperSprite = sprites.get('upper_shield')
            self.source = \
                    (self.location[0] + sourceOffset[0],
                     self.location[1] + sourceOffset[1])

        # Strength of the shield. 1.0 is fully charged.
        self.loadPercent = 0.0

        # True if the beep beep denoting full charge has been played.
        self.loadedPlayed = False

        # If the shield is not local to this computer, it has no way of
        # computing the radius, so it is passed in charge messages and stored.
        self.proxyRadius = 0.0
        self.proxyWidth = 10.0

        self.auraDisplayList = glGenLists(1)
        self.compileList()
        Aura(self)

    def shieldStats(self):
        return self.tower().shieldStats()

    def maxRadius(self):
        return self.shieldStats()['radius']

    def maxStrength(self):
        return self.shieldStats()['strength']

    def strength(self):
        return self.loadPercent * self.maxStrength()

    def center(self):
        x,y = self.source
        offset = 0.5*self.radius()
        if self.side == 'left':
            return (x - cos(shieldAngle)*offset, y - sin(shieldAngle)*offset)
        else:
            return (x + cos(shieldAngle)*offset, y - sin(shieldAngle)*offset)

    def radius(self):
        if self.local():
            return self.loadPercent * self.maxRadius()
        else:
            return self.proxyRadius

    def width(self):
        if self.local():
            return self.shieldStats()['width']
        else:
            return self.proxyWidth


    def loadTime(self):
        return self.shieldStats()['loadTime']

    def complete(self, **msg):
        Mount.complete(self)
        self.loadPercent = 0.0

        # Start charging immediately.
        if self.local():
            self.playPersistentSound('loading_shield')
            self.buildPressed = False
        self.compileList()

    def destroy(self, **msg):
        self.loadPercent = 0.0
        Mount.destroy(self)
        self.compileList()

    def charge(self, percent, radius, width, **msg):
        self.loadPercent = percent
        if not self.local():
            self.proxyRadius = radius
            self.proxyWidth = width
        self.compileList()

    def reshield(self):
        if self.active():
            self.broadcast('charge', percent = self.loadPercent,
                                     radius = self.radius(),
                                     width = self.width())

    def absorb(self, hp):
        loss = float(hp) / float(self.maxStrength())
        percent = max(self.loadPercent - loss, 0.0)
        self.broadcast('charge', percent = percent,
                                 radius = self.maxRadius() * percent,
                                 width = self.width())

    def shieldingAtLocation(self, location):
        if dist2(location, self.center()) < self.radius()**2:
            return self
        else:
            return None

    def hitShield(self, location, radius, damage):
        d = dist(location, self.center()) - self.radius()
        if d < radius:
            self.absorb(damage * (1.0 - float(d)/float(radius))**2)
            return True
        else:
            return False

    def activeClick(self, location):
        if self.loadPercent < 1.0:
            self.playPersistentSound('loading_shield')
        self.loadedPlayed = False

    def activeHold(self, location, duration):
        percent = min(self.loadPercent + float(duration) / self.loadTime(), 1.0)
        self.broadcast('charge', percent = percent,
                                 radius = self.maxRadius() * percent,
                                 width = self.width())
        if percent >= 1.0:
            self.stopPersistentSound()
            if not self.loadedPlayed:
                mixer.play('loaded_shield')
                self.loadedPlayed = True
        
    def activeRelease(self, location):
        self.stopPersistentSound()
        return True

    def compileList(self):
        glNewList(self.auraDisplayList, GL_COMPILE)
        segments = graphicsDependent(15, 20, 25)

        percent = self.loadPercent
        radius = self.radius()
        width = min(self.width(), radius)

        startAngle = shieldAngle - 0.5*shieldAngleSpan
        endAngle = shieldAngle + 0.5*shieldAngleSpan

        glPushMatrix()
        x,y = self.center()
        glTranslatef(x,y,0)
        if self.side == 'right':
            glScalef(-1.0, 1.0, 1.0)

        with NoTexture():
            glBegin(GL_QUAD_STRIP)
            for i in range(segments):
                t = float(i) / float(segments-1)
                angle = interp(t, startAngle, endAngle)
                c, s = cos(angle), sin(angle)

                midAlpha = 1.5 * t * (1.0 - t)

                glColor4f(1,1,1,0)
                glVertex2f((radius - width) * c, (radius - width) * s)
                glColor4f(1,1,1, midAlpha)
                glVertex2f(radius * c, radius * s)
            glEnd()

        glPopMatrix()
        glEndList()

    def drawAura(self):
        if self.active():
            glCallList(self.auraDisplayList)


# Spline {{{1
class Spline():
    def __init__(self, gateway, a, b, c, d):
        self.gateway = gateway
        self.a = a; self.b = b; self.c = c; self.d = d
        self.segments = 30
        self.color = gateway.virusColor()
        self.width = 5

        self.compileList()

        scene.add(self, SPLINE_LAYER)

    def tOfTheWay(self, t):
        a = self.a; b = self.b; c = self.c; d = self.d
        ab = interp(t,a,b); bc = interp(t,b,c); cd = interp(t,c,d)
        abc = interp(t,ab,bc); bcd = interp(t,bc,cd)
        return interp(t, abc, bcd)

    def direction(self, t):
        def sub(u, v):
            return (u[0]-v[0], u[1]-v[1], u[2]-v[2])
        a = self.a; b = self.b; c = self.c; d = self.d
        ab = sub(b,a); bc = sub(b, c); cd = sub(c, d)
        abc = interp(t,ab,bc); bcd = interp(t,bc,cd)
        dir = interp(t, abc, bcd)
        norm = sqrt(dir[0]**2 + dir[1]**2 + dir[2]**2)
        return (dir[0]/norm, dir[1]/norm, dir[2]/norm)

    def drawVirus(self, t, color):
        if t < 1.0:
            alpha = 0.5
        else:
            alpha = 0.5 * (1.0 - (t - 1.0) / virusOverPercent)
        glColor4f(color[0], color[1], color[2], color[3]*alpha)

        size = 20
        with PushedMatrix():
            with Perspective((width/2, height/2)):
                glTranslatef(*self.tOfTheWay(t))
                u = (0,-1,0)
                v = self.direction(t)

                def uv(s, t):
                    return (s*u[0]+t*v[0], s*u[1]+t*v[1], 0)

                # Note that we do not use the usual sprite drawing facilities
                # here.
                sprite = sprites.get('virus')
                lazyBindTexture(sprite.texture)
                glBegin(GL_QUADS)
                sprite.texCoord(0,sprite.height)
                glVertex3fv(uv(-size,-size))
                sprite.texCoord(sprite.width,sprite.height)
                glVertex3fv(uv(size,-size))
                sprite.texCoord(sprite.width,0)
                glVertex3fv(uv(size,size))
                sprite.texCoord(0,0)
                glVertex3fv(uv(-size,size))
                glEnd()

    def compileList(self):
        self.displayList = glGenLists(1)
        glNewList(self.displayList, GL_COMPILE)
        segments = self.segments
        r,g,b,a = self.color
        halfwidth = self.width / 2.0
        with Perspective((width/2, height/2)):
            with NoTexture():
                glBegin(GL_QUAD_STRIP)
                for i in range(segments):
                    t = float(i) / float(segments-1)
                    
                    x,y,z = self.tOfTheWay(t)

                    glColor4f(r,g,b,0)
                    glVertex3f(x,y-halfwidth,z)
                    glColor4f(r,g,b,a*t*(1.0-t))
                    glVertex3f(x,y,z)
                glEnd()
                glBegin(GL_QUAD_STRIP)
                for i in range(segments):
                    t = float(i) / float(segments-1)
                    
                    x,y,z = self.tOfTheWay(t)

                    glColor4f(r,g,b,a*t*(1.0-t))
                    glVertex3f(x,y,z)
                    glColor4f(r,g,b,0)
                    glVertex3f(x,y+halfwidth,z)
                glEnd()
        glEndList()

    def draw(self):
        if self.gateway.active():
            color = self.gateway.virusColor()
            if self.color != color:
                self.color = color
                self.compileList()

            glCallList(self.displayList)

    def doneDrawing(self):
        return self.gateway.doneDrawing()

# Virus {{{1
class Virus(Entity):
    def __init__(self, side, level, gateway, target, **msg):
        self.side = side
        self.level = level
        self.gateway = gateway
        self.target = target

        self.startTicks = updater.get_ticks()
        self.exploded = False

        scene.add(self, VIRUS_LAYER)
        if self.local():
            updater.add(self, 0)
        entities[gateway].broadcast('fire')

        self.splines = entities[gateway].splines
        mixer.play('virus')

    def percent(self):
        p = float(updater.get_ticks() - self.startTicks) / virusTravelTime
        if p < 0.0:
            return 0.0
        else:
            return p

    def virusColor(self):
        if otherSide(self.side) in towers:
            return virusColors[self.level]
        else:
            return foreignVirusColor

    def draw(self):
        p = self.percent()
        if p < 1.0 + virusOverPercent:
            for spline in self.splines:
                spline.drawVirus(p, self.virusColor())

    def doneDrawing(self):
        return self.percent() > 1.0 + virusOverPercent

    def update(self, ticks, duration):
        if self.percent() >= 1.0 and not self.exploded:
            self.exploded = True

            # Get the building at the target, and tell it has a virus
            obj = targets.objectAtLocation(self.target)
            if obj and obj.side == self.side:
                # Don't broadcast a message.  The building will broadcast a
                # destroy message on a successsful infection.
                obj.infect(self.level) # Add level of virus.

# Gateway {{{1
class Gateway(SpriteBuilding):
    def __init__(self, name, side, location, **msg):
        SpriteBuilding.__init__(self, name, 'gateway', side, location, **msg)
        self.createSplines()

        self.virusSprite = sprites.get('virus')

        # Note some of this code could be shared with Gun, n'est pas?
        self.gatewayState = 'loading'
        self.firePressed = False
        self.loadPercent = 0.0

    def createSplines(self):
        self.splines = []

        spreadx = 50.0; spready = 150.0
        sw = self.sprite.width; sh = self.sprite.height
        source = (self.location[0]+sw/2.0, self.location[1] + sh/2.0)
        self.source = source

        if self.side == 'left':
            target = (width - 2*spreadx, 2*spready)
        else:
            target = (2*spreadx, 2*spready)

        for i in range(graphicsDependent(6,8,10)):
            randx = random.gauss(0.0, 1.0)
            randy = random.gauss(0.0, 1.0)
            self.addSpline((source[0] + 0.1*randx*sw,
                               source[1] + 0.1*randy*sh),
                           (target[0] + randx*spreadx,
                               target[1] + randy*spready))

    def addSpline(self, source, target):
        x0, y0 = source
        x3, y3 = target

        a = (x0, y0, width/2)
        b = (interp(0.2, x0, x3), interp(0.2, y0, y3) + 500, width)
        c = (interp(0.7, x0, x3), interp(0.7, y0, y3) - 200, 0.75*width)
        d = (x3, y3, width/2)
        self.splines.append(Spline(self, a, b, c, d))

    def desc(self):
        if self.gatewayState == 'aiming':
            return "Click on target to infect."
        else:
            return SpriteBuilding.desc(self)

    def virusColor(self):
        if self.local():
            return self.tower().virusColor()
        else:
            return foreignVirusColor

    def draw(self):
        SpriteBuilding.draw(self)

        # Draw the loading of the virus.
        if self.active():
            percent = self.loadPercent
            if percent > 0.0:
                with PushedMatrix():
                    glTranslatef(self.source[0], self.source[1], 0)
                    glScalef(percent, percent, percent)

                    r,g,b,a = self.virusColor()
                    if percent < 1.0:
                        glColor4f(r,g,b,0.3)
                    else:
                        glColor4f(r,g,b,0.5)

                    self.virusSprite.drawCentered(0)

    def complete(self, **msg):
        SpriteBuilding.complete(self)

        self.buildPressed = False
        if self.local():
            self.playPersistentSound('loading_virus')

    def destroy(self, **msg):
        if self.gatewayState == 'aiming' and self.playing():
            pygame.mouse.set_cursor(*pygame.cursors.arrow)
            clickables.unselect()
        self.loadPercent = 0.0
        self.gatewayState = 'loading'

        SpriteBuilding.destroy(self)

    def fire(self, **msg):
        self.gatewayState = 'loading'
        self.loadPercent = 0.0

    def activeClick(self, location):
        if self.gatewayState == 'aiming' and \
                sideOfScreen(location) != self.side:
            self.firePressed = True
            if self.playing():
                pygame.mouse.set_cursor(*pygame.cursors.arrow)
            createEntity(Virus,
                    side = otherSide(self.side),
                    level = self.tower().virusLevel,
                    gateway = self.name,
                    target = location) # To add, level of virus.
        elif self.gatewayState == 'loading':
            self.playPersistentSound('loading_virus')

    def activeHold(self, location, duration):
        if self.gatewayState == 'loading' and not self.firePressed:
            self.loadPercent += float(duration) / gatewayLoadTime
            if self.loadPercent >= 1.0:
                self.loadPercent = 1.0
                self.gatewayState = 'loaded'
                self.stopPersistentSound()
                mixer.play('loaded_virus')
    
    def release(self, location):
        if self.firePressed == True:
            self.firePressed = False
        return SpriteBuilding.release(self, location)

    def activeRelease(self, location):
        if self.gatewayState == 'loaded':
            self.gatewayState = 'aiming'
            if self.playing():
                pygame.mouse.set_cursor(*pygame.cursors.broken_x)
            clickables.updateMessageBar()
            return False
        elif self.gatewayState == 'loading':
            self.stopPersistentSound()
            return True
        else:
            assert self.gatewayState == 'aiming'
            return self.firePressed

# Mutator {{{1
class Mutator(SpriteBuilding):
    def __init__(self, name, side, location, **msg):
        SpriteBuilding.__init__(self, name, 'mutator', side, location, **msg)
        self.mutating = False
        self.mutated = False
        self.waitTime = None

    def color(self):
        if self.active():
            if self.mutating:
                return (random.uniform(0.0, 1.0),
                        random.uniform(0.0, 1.0),
                        random.uniform(0.0, 1.0),
                        1.0)
            elif self.mutated:
                return self.tower().virusColor()
        return SpriteBuilding.color(self)

    def activeClick(self, location):
        self.mutating = True
        self.waitTime = random.expovariate(log(2) / mutatorHalfLife)
        self.playPersistentSound('mutator')

    def activeHold(self, location, duration):
        if self.mutating and duration > 0:
            self.waitTime -= duration
            if self.waitTime <= 0:
                # We have achieved mutation
                self.mutating = False
                self.waitTime = None
                self.mutated = True
                self.tower().evolveVirus()
                self.stopPersistentSound()
                mixer.play('mutated')

    def activeRelease(self, location):
        self.mutating = False
        self.waitTime = None
        self.mutated = False
        self.stopPersistentSound()
        return True

# Girder {{{1
class Girder(Building):
    """Levels make up the tower that we are building, and each level is
    supported by a girder, a building built on one level that allows us to use
    the next level."""

    def __init__(self, name, tier, side, y, length, **msg):
        self.tier = tier
        self.length = length
        self.girder = sprites.get('girder')

        if side == 'left':
            x = 0
        else:
            assert side == 'right'
            x = width - length   # This is the global width of the screen.

        self.location = (x,y)
        bbox = (x, y+levelHeight-self.girder.height,
                x+length, y+levelHeight)
        Building.__init__(self, name, 'girder', side, bbox)

    def infect(self, level):
        # Girders are immune to virii.
        pass

    def complete(self, sound = 'built', **msg):
        if self.tier == len(levelStats)-1:
            sound = 'victory'
        Building.complete(self, sound, **msg)

    def protection(self):
        """Return the number of hit points of protection this level has before
        it will collapse."""
        t,l,b,r = self.bbox
        shield = self.tower().shieldingAtLocation((t,l))
        return (shield.strength() if shield else 0) + self.hp

    def drawPillar(self, location, percent):
        """Draw a pillar based at location.  percent is the amount of the pillar
        that has been built (from 0 to 1)."""
        strutHeight = 24; gap = 0
        x,y = location

        box = (x-pillarWidth/2, y+gap,
               x+pillarWidth/2, y+levelHeight-strutHeight-gap)

        if self.state == 'building':
            if percent <= 0.0:
                drawRectangle(box, buildingColor)
            elif percent >= 1.0:
                drawRectangle(box, builtColor)
            else:
                l,b,r,t = box
                y = b + percent * (t-b)
                drawRectangle((l,b,r,y), builtColor)
                drawRectangle((l,y,r,t), buildingColor)
        else:
            color = self.color()
            if color:
                drawRectangle(box, color)

    def drawStrut(self, percent):
        x,y = self.location; length = self.length
        if self.side == 'left':
            xx = x+length-self.girder.width
            flipped = False
        else:
            xx = x
            flipped = True
        loc = (xx, y+levelHeight-self.girder.height)

        if self.state == 'building':
            if percent <= 0.0:
                glColor4fv(buildingColor)
                self.girder.draw(0, loc, flipped)
            elif percent >= 1.0:
                glColor4fv(builtColor)
                self.girder.draw(0, loc, flipped)
            else:
                # Adjust the percentage for the portion shown on screen.
                sp = 1.0 - (1.0 - percent) * float(length) / \
                                             float(self.girder.width)
                if self.side == 'left':
                    glColor4fv(builtColor)
                    self.girder.drawPartial(0, loc, (0,0,sp,1))
                    if self.local():
                        glColor4fv(buildingColor)
                        self.girder.drawPartial(0, loc, (sp,0,1,1))
                else:
                    glColor4fv(builtColor)
                    self.girder.drawPartial(0, loc, (1.0-sp,0,1,1), True)
                    if self.local():
                        glColor4fv(buildingColor)
                        self.girder.drawPartial(0, loc, (0,0,1.0-sp,1), True)
        else:
            color = self.color()
            if color:
                glColor4fv(color)
                self.girder.draw(0, loc, flipped)

    def draw(self):
        x,y = self.location; length = self.length
        percent = self.percentComplete()

        if self.side == 'left':
            p1 = percent*2
            p2 = (percent-0.365)*2
        else:
            p1 = (percent-0.365)*2
            p2 = percent*2


        self.drawPillar((x + pillarInset*self.length, y), p1)
        self.drawPillar((x + (1.0-pillarInset)*self.length, y), p2)

        self.drawStrut((percent-0.4)/0.6)

# Level {{{1

class Level:
    """Containter for all of the buildings on one level of the tower."""
    def __init__(self, side, tier, location, length):
        self.side = side
        self.tier = tier
        self.location = location
        self.length = length

        self.buildings = []

        self.createGirder()
        self.createStrategicBuildings()
        self.createMounts()

    def addBuilding(self, building):
        self.buildings.append(building)

    def numberOf(self, model):
        num = 0
        for building in self.buildings:
            if building.model == model and building.active():
                num += 1
        return num

    def buildingsByModel(self, model, state=None):
        return [building for building in self.buildings \
                if building.shown and \
                (model==None or building.model == model) and \
                (state==None or building.state == state)]

    def strategicBuildings(self, state=None):
        return [building for building in [self.strat1, self.strat2] \
                if building.shown and \
                (state==None or building.state == state)]

    def shieldingAtLocation(self, location):
        if self.lowerShield.active() and \
                self.lowerShield.shieldingAtLocation(location):
            return self.lowerShield
#        elif self.upperShield.active() and \
#                self.upperShield.shieldingAtLocation(location):
#            return self.upperShield
        else:
            return None

    def hitShield(self, location, radius, damage):
        if self.lowerShield.active() and \
                self.lowerShield.hitShield(location, radius, damage):
            return True
#        elif self.upperShield.active() and \
#                self.upperShield.hitShield(location, radius, damage):
#            return True
        else:
            return False

    def reshield(self):
        """Recompute the radii of shields, due to a change in the number of
        power plants."""
        self.lowerShield.reshield()
#        self.upperShield.reshield()

    def createGirder(self):
        x,y = self.location
        strutLength = self.length - levelStep
        self.girder = createEntity(Girder,
                                   side = self.side,
                                   tier = self.tier,
                                   y = y,
                                   length = strutLength)
        self.girder.stats = levelStats[self.tier]['girder']
        self.addBuilding(self.girder)


    def createStrategicBuildings(self):
        x,y = self.location
        strutLength = self.length - levelStep
        center = int(strutLength / 2)
        if self.side == 'right':
            center += width - strutLength

        buildingNames = levelStats[self.tier]['buildings']
        bw = [buildingWidth(name) for name in buildingNames]

        overlap = 32
        left = center - (bw[0] + bw[1] - overlap) / 2
        right = left + bw[0] - overlap

        building = createEntity(buildingNames[0],
                                side = self.side,
                                location = (left, y))
        self.addBuilding(building)
        self.strat1 = building

        building = createEntity(buildingNames[1],
                                side = self.side,
                                location =
                                    (right, y))
        self.addBuilding(building)
        self.strat2 = building

        self.strat1.alternate = self.strat2
        self.strat2.alternate = self.strat1

    def createMounts(self):
        self.createLowerMount()
        self.createUpperMount()

        self.lowerShield.alternate = self.upperGun
        self.upperGun.alternate = self.lowerShield

    def createLowerMount(self):
        x,y = self.location
        if self.side == 'left':
            lowerMount = (self.length - mountLength, y)
        else:
            lowerMount = (width - self.length, y)

#        self.lowerGun = createEntity(Gun,
#                                     side = self.side,
#                                     location = lowerMount,
#                                     upper = False)
#        self.addBuilding(self.lowerGun)
        self.lowerShield = createEntity(Shield,
                                        side = self.side,
                                        location = lowerMount,
                                        upper = False)
        self.addBuilding(self.lowerShield)

#        self.lowerGun.alternate = self.lowerShield
#        self.lowerShield.alternate = self.lowerGun

    def createUpperMount(self):
        x,y = self.location
        if self.side == 'left':
            upperMount = (self.length - 2*mountLength, y + mountHeight)
        else:
            upperMount = (width - self.length + mountLength, y + mountHeight)

        self.upperGun = createEntity(Gun,
                                     side = self.side,
                                     location = upperMount,
                                     upper = True)
        self.addBuilding(self.upperGun)
#        self.upperShield = createEntity(Shield,
#                                        side = self.side,
#                                        location = upperMount,
#                                        upper = True)
#        self.addBuilding(self.upperShield)

#        self.upperGun.alternate = self.upperShield
#        self.upperShield.alternate = self.upperGun

    def float(self):
        for building in self.buildings:
            building.broadcast('float')

    def erase(self, building):
        self.buildings.remove(building)
        building.broadcast('erase')

# Tower {{{1
class Tower:
    def __init__(self, side):
        self.side = side
        self.playing = True
        self.levels = []
        self.auditing = False

        self.handicap = shelf['handicap']

        # How immune is this tower to virii?
        self.immunityLevel = -1

        # How evolve are it's virii?
        self.virusLevel = 0

    def startup(self):
        # Draw an indestructable base girder.  Buildings can't be created before
        # the towers dictionary is created, which is why this is not in
        # __init__.
        self.base = createEntity(Girder,
                                 side = self.side,
                                 tier = -1,
                                 y = baseHeight-levelHeight,
                                 length = baseLength)
        self.base.broadcast('complete', sound = None)
        targets.remove(self.base)
        clickables.remove(self.base)
        updater.remove(self.base)

        self.audit()

    def addLevel(self):
        n = len(self.levels)
        self.levels.append(Level(self.side, n,
                                 (0, baseHeight + n * levelHeight),
                                 baseLength - n * levelStep))

    def numberOf(self, model):
        return reduce(add, [level.numberOf(model) for level in self.levels], 0)

    def buildingsByModel(self, model, state=None):
        return reduce(concat,
                      [level.buildingsByModel(model, state) \
                              for level in self.levels],
                      [])

    def strategicBuildings(self, state=None):
        return reduce(concat,
                      [level.strategicBuildings(state) \
                              for level in self.levels],
                      [])

    def weakestGirder(self):
        girders = self.buildingsByModel('girder', 'built')
        if girders:
            return argmin(girders, Girder.protection)
        else:
            return None

    def shieldingAtLocation(self, location):
        for level in self.levels:
            shield = level.shieldingAtLocation(location)
            if shield:
                return shield
        return None

    def hitShield(self, location, radius, damage):
        for level in self.levels:
            if level.hitShield(location, radius, damage):
                return True
        return False

    def reshield(self):
        for level in self.levels:
            level.reshield()

    def factoryBoost(self):
        return factoryBoost[self.numberOf('factory')]

    def missileStats(self):
        return missileStats[self.numberOf('munitions')]

    def shieldStats(self):
        return shieldStats[self.numberOf('plant')]
    def virusColor(self):
        return virusColors[self.virusLevel]

    def evolveVirus(self):
        self.virusLevel += 1

        # Invent virus colors if we have run out of pre-defined colors.
        while self.virusLevel >= len(virusColors):
            virusColors.append((random.uniform(0.5, 1.0),
                                random.uniform(0.5, 1.0),
                                random.uniform(0.5, 1.0),
                                1.0))
    def infect(self, level):
        """Updates the immunity level of the tower, and returns True if the
        infection is destructive, and will destroy the infected building."""
        if level <= self.immunityLevel:
            return False
        else:
            # The infection is destructive with a certain probability.
            if random.uniform(0.0, 1.0) < 0.75:
                # And is destructive infection builds an immunity with a certain
                # probability.
                if random.uniform(0.0, 1.0) < 0.40:
                    self.immunityLevel = level
                return True
            else:
                return False

    def audit(self):
        """When a girder is completed, this is called to create new
        blueprints for building on the new level. When a girder is destroyed,
        this is called to remove unsupported buildings."""
        if not self.auditing:
            self.auditing = True
            try:
                if self.levels == []:
                    self.levels.append(Level(self.side, 0,
                                             (0, baseHeight),baseLength))
                else:
                    levels = self.levels
                    for i in range(len(levels)-1):
                        if not levels[i].girder.active():
                            # Remove and float the above levels.
                            self.levels = levels[:i+1]
                            for j in range(i+1,len(levels)):
                                levels[j].float()
                            return

                    if levels[-1].girder.active():
                        if len(levels) >= len(levelStats):
                            # We have built the highest level, and therefore
                            # won.
                            createEntity(VictoryMessage, side = self.side)
                        else:
                            # More work to go.
                            self.addLevel()

            finally:
                self.auditing = False

towers = {}
def createTower(side):
    assert side not in towers
    towers[side] = Tower(side)
    towers[side].startup()

# Commands {{{1
class Command:
    def __init__(self, tower):
        self.tower = tower
    def update(self, duration):
        pass

nullLocation = (0,0)

class BuildCommand(Command):
    def __init__(self, tower, buildings):
        """Buildings is a list of possible buildings to build.  One is chosen at
        random."""
        Command.__init__(self, tower)
        if buildings == []:
            self.building = None
        else:
            self.building = random.choice(buildings)

    def possible(self):
        return self.building != None
    def start(self):
        self.building.click(nullLocation)
    def update(self, duration):
        self.building.hold(nullLocation, duration)
    def done(self):
        return self.building.state != 'building'
    def complete(self):
        self.building.release(nullLocation)
        
class BuildGirderCommand(BuildCommand):
    def __init__(self, tower):
        BuildCommand.__init__(self, tower, [tower.levels[-1].girder])
    def possible(self):
        return len(self.tower.levels) <= len(levelStats) and \
                BuildCommand.possible(self)
    def done(self):
        if BuildCommand.done(self):
            return True
        # Return premateurly if the rest of the tower is in danger.
        girder = self.tower.weakestGirder()
        if girder and girder.protection() < 800:
            return True
        return False

class BuildGunCommand(BuildCommand):
    def __init__(self, tower):
        BuildCommand.__init__(self, tower,
                tower.buildingsByModel('gun', 'blueprint'))

class BuildShieldCommand(BuildCommand):
    def __init__(self, tower):
        BuildCommand.__init__(self, tower,
                tower.buildingsByModel('shield', 'blueprint'))

class BuildStrategicBuildingCommand(BuildCommand):
    def __init__(self, tower):
        BuildCommand.__init__(self, tower,
                tower.strategicBuildings('blueprint'))

class FireCommand(Command):
    def targetBuildings(self):
        return self.tower.enemy().buildingsByModel(None, 'built')

    def target(self):
        buildings = self.targetBuildings()
        if buildings == []:
            return (100,200)
        else:
            building = random.choice(buildings)
            return bboxCenter(building.bbox)

    def possible(self):
        return self.gun != None

    def start(self):
        self.gun.click(nullLocation)
    def update(self, duration):
        if self.gun.loadPercent < 1.0:
            self.gun.hold(nullLocation, duration)
        elif not self.fired:
            self.gun.release(nullLocation)
            self.gun.click(self.target())
            self.gun.release(nullLocation)
            self.fired = True
    def done(self):
        return self.gun.state != 'built' or self.fired
    def complete(self):
        pass

class FireGunCommand(FireCommand):
    def __init__(self, tower):
        FireCommand.__init__(self, tower)
        self.fired = False

        guns = tower.buildingsByModel('gun', 'built')
        if guns == []:
            self.gun = None
        else:
            self.gun = random.choice(guns)

class FireVirusCommand(FireCommand):
    def __init__(self, tower):
        FireCommand.__init__(self, tower)
        self.fired = False

        guns = tower.buildingsByModel('gateway', 'built')
        if guns == []:
            self.gun = None
        else:
            self.gun = random.choice(guns)

    def possible(self):
        return FireCommand.possible(self) and self.tower.virusUsage < 3

    def targetBuildings(self):
        return [b for b in FireCommand.targetBuildings(self) \
                if b.model != 'girder']

    def complete(self):
        FireCommand.complete(self)
        self.tower.virusUsage += 1

class ChargeShieldCommand(Command):
    def __init__(self, tower):
        Command.__init__(self, tower)
        self.timeout = 5000
        shields = tower.buildingsByModel('shield', 'built')
        if shields == []:
            self.shield = None
        else:
            self.shield = random.choice(shields)

    def possible(self):
        return self.shield != None

    def start(self):
        self.shield.click(nullLocation)
    def update(self, duration):
        self.shield.hold(nullLocation, duration)
        self.timeout -= duration
    def done(self):
        return self.shield.loadPercent >= 1.0 or self.timeout < 0
    def complete(self):
        self.shield.release(nullLocation)

class MutateCommand(Command):
    def __init__(self, tower):
        Command.__init__(self, tower)
        mutators = tower.buildingsByModel('mutator', 'built')
        if mutators == []:
            self.mutator = None
        else:
            self.mutator = random.choice(mutators)
        self.timeout = 5000
        self.level = self.tower.virusLevel

    def possible(self):
        return self.mutator != None and self.tower.virusUsage > 0

    def start(self):
        self.mutator.click(nullLocation)
    def update(self, duration):
        self.mutator.hold(nullLocation, duration)
        self.timeout -= duration
    def done(self):
        return self.timeout < 0 or self.tower.virusLevel != self.level
    def complete(self):
        self.mutator.release(nullLocation)

commandList = [BuildGirderCommand] + \
              [BuildGunCommand] * 4 + \
              [BuildShieldCommand] * 2 + \
              [BuildStrategicBuildingCommand] * 2 + \
              [FireGunCommand] * 4 + \
              [ChargeShieldCommand] * 4 + \
              [FireVirusCommand] * 6 + \
              [MutateCommand] * 4

# IntelligentTower {{{1
class IntelligentTower(Tower):
    def __init__(self, side):
        Tower.__init__(self, side)
        self.playing = False
        self.handicap = shelf['aiHandicap']

        updater.add(self, 0)

        self.command = None

        # Number of times a virus of a particular strain has been used.  Used
        # to stop the AI from sending out the same virus again and again.
        self.virusUsage = 0

    def enemy(self):
        return towers[otherSide(self.side)]

    def evolveVirus(self):
        Tower.evolveVirus(self)
        self.virusUsage = 0

    def chooseCommand(self):
        if self.command:
            self.command.complete()
            self.command = None

        for i in range(10):
            cmd = random.choice(commandList)
            self.command = cmd(self)
            if self.command.possible():
                self.command.start()
                return

        self.command = None

    def update(self, ticks, duration):
        if (self.command == None or self.command.done()) and victor == None:
            self.chooseCommand()
        if self.command:
            self.command.update(duration)


# Menu {{{1
def startGame(sides):
    title.hide()
    for side in sides:
        createTower(side)

def menuLocation(row, justification='center'):
    x = width/2
    j = justification
    if j == 'left':
        x -= 100
    elif j == 'right':
        x += 100
    else:
        assert j in ['center', 'centre']

    y = height - 200 - 40 * row

    return (x,y)

def hideMenu(menu):
    for widget in menu:
        widget.hide()

def showMenu(menu):
    for widget in menu:
        widget.show()

def singlePlayerCallback():
    hideMenu(mainMenu)
    title.hide()
    towers['left'] = Tower('left')
    towers['left'].startup()
    towers['right'] = IntelligentTower('right')
    towers['right'].startup()

def startDemo():
    hideMenu(mainMenu)
    title.hide()
    towers['left'] = IntelligentTower('left')
    towers['left'].startup()
    towers['right'] = IntelligentTower('right')
    towers['right'].startup()

class WaitForClient():
    def update(self, ticks, duration):
        if net.connectToClient():
            updater.remove(self)
            hideMenu(serverMenu)

            startGame(['left'])
waitForClient = WaitForClient()

def hostGameCallback():
    hideMenu(mainMenu)
    showMenu(serverMenu)

    # TODO: Add exception catching.
    net.startServer()

    serverStatusLabel.text = "Hosting game at "+net.server.getsockname()[0]

    updater.add(waitForClient, -99)

def cancelServerCallback():
    hideMenu(serverMenu)
    showMenu(mainMenu)

    updater.remove(waitForClient)
    net.reset()

class WaitForServer():
    def __init(self):
        self.pooched = False

    def update(self, ticks, duration):
        assert 'address' in self.__dict__
        try:
            if net.connectToServer(self.address):
                updater.remove(self)
                hideMenu(clientMenu)

                # This starts the action.
                startGame(['right'])
        except socket.error, e:
            clientStatusLabel.text = "Error: "+e[1]
waitForServer = WaitForServer()

def connectToCallback(address):
    # Store address between runs.
    shelf['address'] = address

    hideMenu(mainMenu)
    showMenu(clientMenu)

    waitForServer.address = address
    updater.add(waitForServer, -99)

def cancelClientCallback():
    hideMenu(clientMenu)
    showMenu(mainMenu)

    updater.remove(waitForServer)
    waitForServer.pooched = False
    del waitForServer.address
    net.reset()

def playgroundCallback():
    hideMenu(mainMenu)
    title.hide()
    startGame(['left', 'right'])

def settingsCallback():
    hideMenu(mainMenu)
    showMenu(settingsMenu)

def handicapCallback(value):
    try:
        handicap = float(value)
        shelf['handicap'] = handicap
    except ValueError:
        handicapBox.text = str(shelf['handicap'])

def aiHandicapCallback(value):
    try:
        handicap = float(value)
        shelf['aiHandicap'] = handicap
    except ValueError:
        aiHandicapBox.text = str(shelf['aiHandicap'])

def graphicsLevelCallback(value):
    try:
        level = int(value)
        shelf['graphicsLevel'] = max(1, min(3, level))
    except ValueError:
        pass
    graphicsLevelBox.text = str(shelf['graphicsLevel'])

def settingsReturnCallback():
    hideMenu(settingsMenu)
    showMenu(mainMenu)

def initMenus():
    global mainMenu, serverMenu, clientMenu, settingsMenu
    global singlePlayerButton, hostGameButton, connectToLabel, addressBox, \
            playgroundButton, settingsButton
    
    global serverStatusLabel, cancelServerButton
    global clientStatusLabel, cancelClientButton
    global handicapLabel, handicapBox, graphicsLevelLabel, \
            aiHandicapLevel, aiHandicapBox, \
            graphicsLevelBox, settingsReturnButton

    singlePlayerButton = Button('Single Player', menuLocation(0),
            singlePlayerCallback)
    hostGameButton = Button('Host Game', menuLocation(2), hostGameCallback)
    connectToLabel = Label('Connect to:', menuLocation(3))
    addressBox = TextBox(menuLocation(4), 40, '127.0.1.1', connectToCallback)
    if 'address' in shelf:
        addressBox.text = shelf['address']
    playgroundButton = Button('Playground', menuLocation(6), playgroundCallback)
    settingsButton = Button('Settings', menuLocation(8), settingsCallback)

    mainMenu = [singlePlayerButton, hostGameButton, connectToLabel, addressBox,
            playgroundButton, settingsButton]

    serverStatusLabel = Label('Started server.', menuLocation(0))
    cancelServerButton = Button('Cancel', menuLocation(1, 'right'),
            cancelServerCallback)

    serverMenu = [serverStatusLabel, cancelServerButton]

    clientStatusLabel = Label('Connecting to server.', menuLocation(0))
    cancelClientButton = Button('Cancel', menuLocation(1, 'right'),
            cancelClientCallback)

    clientMenu = [clientStatusLabel, cancelClientButton]

    graphicsLevelLabel = Label('Graphics Level:', menuLocation(0))
    graphicsLevelBox = TextBox(menuLocation(1), 12,
            str(shelf['graphicsLevel']), graphicsLevelCallback)
    handicapLabel = Label('Player Handicap:', menuLocation(2))
    handicapBox = TextBox(menuLocation(3), 12,
            str(shelf['handicap']), handicapCallback)
    aiHandicapLabel = Label('AI Handicap:', menuLocation(4))
    aiHandicapBox = TextBox(menuLocation(5), 12,
            str(shelf['aiHandicap']), aiHandicapCallback)
    settingsReturnButton = Button('Return', menuLocation(6),
            settingsReturnCallback)

    settingsMenu = [handicapLabel, handicapBox, graphicsLevelLabel,
            graphicsLevelBox, aiHandicapLabel, aiHandicapBox,
            settingsReturnButton]

    hideMenu(mainMenu)
    hideMenu(serverMenu)
    hideMenu(clientMenu)
    hideMenu(settingsMenu)

postGLInits.append(initMenus)

def showMainMenu():
    showMenu(mainMenu)

# Basic GL Functions {{{1
def resize((width, height)):
    if height == 0:
        height = 1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, width, 0.0, height, -1.0, 1.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def init():
    global textureFlag
    if glInitTextureRectangleARB():
        textureFlag = GL_TEXTURE_RECTANGLE_ARB
    else:
        textureFlag = GL_TEXTURE_2D
    glEnable(textureFlag)
    glShadeModel(GL_SMOOTH)
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glEnable(GL_BLEND)

    # Call all of the init functions that have been waiting for OpenGL to be
    # initialized.
    for func in postGLInits:
        func()

def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()

    scene.draw()

def resetDisplay():
    video_flags = OPENGL | DOUBLEBUF
    if shelf['fullscreen']:
        video_flags |= FULLSCREEN | HWSURFACE

    pygame.display.set_mode((width,height), video_flags)

    resize((width,height))

def toggleFullscreen():
    shelf['fullscreen'] = not shelf['fullscreen']
    resetDisplay()

# Event Handling Code {{{1
def onKeyDown(event):
    global square
    if event.key == K_ESCAPE:
        return 0
    elif clickables.key(event.unicode, event.key, event.mod):
        return 1
    elif event.key == K_SPACE:
        createEntity(PauseMessage, pause = not updater.paused)
        #updater.togglePause()
        return 1
    elif event.key == K_f:
        toggleFPS()
        return 1
    elif event.key == K_m:
        mixer.toggleMusic()
        return 1
    elif event.key == K_d:
        if len(towers) == 0:
            startDemo()
        return 1
    elif event.key == K_w:
        toggleFullscreen()
        return 1
    else:
        return 1
    
def onMouseDown(event, ticks):
    if event.button == 1:
        clickables.click(invertPos(event.pos), updater.convert(ticks))

def onMouseMotion(event, ticks):
    clickables.move(invertPos(event.pos), updater.convert(ticks))

def onMouseUp(event, ticks):
    if event.button == 1:
        clickables.release(invertPos(event.pos), updater.convert(ticks))

def onUserEvent(event, ticks):
    if event.type == PURGE_EVENT:
        scene.purge()
        showFPS()
    else:
        assert event.type == MUSIC_EVENT
        mixer.newSong()

# Main {{{1
def main():
    pygame.init()
    pygame.time.set_timer(PURGE_EVENT, 2000)

    resetDisplay()

    resize((width,height))
    init()

    showMainMenu()

    frames = 0
    startTicks = pygame.time.get_ticks()
    lastTicks = startTicks
    while 1:
        event = pygame.event.poll()

        ticks = pygame.time.get_ticks()
        clock.tick()

        if event.type == QUIT:
            break
        if event.type == KEYDOWN:
            if onKeyDown(event) == 0:
                break
        if event.type == MOUSEBUTTONDOWN:
            onMouseDown(event, ticks)
        if event.type == MOUSEMOTION:
            onMouseMotion(event, ticks)
        if event.type == MOUSEBUTTONUP:
            onMouseUp(event, ticks)
        if event.type >= USEREVENT:
            onUserEvent(event, ticks)

        duration = ticks - lastTicks
        if duration > updateThreshold:
            updater.update(ticks, duration)
            lastTicks = ticks

        processPendingMessages()

        draw()
        pygame.display.flip()
        frames += 1

    #print "fps:    %d" % ((frames*1000)/(pygame.time.get_ticks()-startTicks))

if __name__ == '__main__': main()
